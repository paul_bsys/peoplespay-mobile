/* eslint-disable prettier/prettier */
import environment from './environment';

let baseUrl = '';
switch (environment.prod) {
  case 'TEST':
    baseUrl = 'http://test.peoplepay.com.gh:9000/peoplepay';
    break;
  case 'PROD':
    baseUrl = 'https://peoplespay.com.gh/peoplepay';
    break;
  default:
    baseUrl = 'http://localhost:3500/peoplepay';
    break;
}

export const evoucherUrl = 'http://34.231.247.199:66/gvivewar';
export const BVIRTUAL = 'https://bvirtualcard.com/api2';
export default baseUrl;
