/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState,useEffect, useContext } from 'react';
import { View,StyleSheet,Dimensions,ScrollView,Text,Modal, Alert, Image, TextInput, TouchableOpacity } from 'react-native';
import Header from '../../components/Header';
import IconButton from '../../components/IconButton';
import LazyArrowButton from '../../components/LazyArrowButton';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import TxtInput from '../../components/TxtInput';
import { ReferralConsumer } from '../../states/referrals.state';
import Clipboard from '@react-native-community/clipboard';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from "react-native-vector-icons/FontAwesome5";
import colors from '../../constants/colors';
import BatchedBridge from 'react-native/Libraries/BatchedBridge/BatchedBridge';
import service from '../../services/referral.service'
import { AuthContext } from "../../states/auth.state";

const {height,width} = Dimensions.get('window');

const ReferalsComponent = (props)=>{

    let _generateCode = null;
    let _loadReferal = null;
    let _deleteCode = null;
    let code = '';

    const [rCode, setRCode] = useState('')

    const authContext = useContext(AuthContext)
    const id = authContext.state.user._id
    const codeApplied = authContext.state.user.codeApplied


    useEffect(()=>{
        _loadReferal();
    });


    const share = (code)=>{
        try {
            Clipboard.setString(code);
            Alert.alert(code,'Your referral code is copied to clipboard,you can paste and share any where with your friends');
        } catch (err) {
            // console.log(err);
        }
    };

    const redeem = async () => {
        try {
            let data ={
                id:id,
                code: rCode
            }

            if (rCode === '') {
                return Alert.alert('Oops!', 'Enter code to confirm')
            }
    
            let res = await service.redeemCode(data)
                if (!res.success) {
                    return Alert.alert('Oops!', 'There was a problem redeeming your code')
                }
                setRCode('')
                Alert.alert('Yay!', 'Your code was redeemed successfully')
        } catch (error) {
            console.log('REDEEMING ERROR:  ', error);
            return Alert.alert('Oops!', 'There was a problem redeeming your code')

        }
        
    }

    const removeReferalCode = ()=>Alert.alert(
        'Confirm Delete',
        'Do you want to delete your referral code? you will no longer receive rewards from this code by friends',
        [
            {
                text:'Yes,Delete',
                onPress:_deleteCode,
            },
            {
                text:'No,Cancel',
                onPress:null,
            },
        ]
    );

    return (
        <ReferralConsumer>
            {
                ({state,loadReferal,deleteCode})=>{
                    // _generateCode = generateCode;
                    _loadReferal = loadReferal;
                    _deleteCode = deleteCode;
                    return (
                        <>
                            <Header text="Referals" pressTo={()=>props.navigation.goBack()}/>
                            <View style={styles.container}>

                                <View
                                style={{
                                    alignItems:'center',
                                    width:'100%',
                                    marginBottom:60,


                                }}
                                >
                                    <Image source={require('../../assets/images/pp.png')} style={{height:100, width:100}} />

                                    {(codeApplied === undefined || codeApplied === false ) && 

                                        <View
                                        style={{
                                            flexDirection:'row',
                                            marginTop:30,
                                            alignItems:'center',
                                            borderBottomWidth:2,
                                            borderBottomColor:'lightgray',
                                            
                                        }}
                                        >
                                            <TextInput 
                                                placeholder='Enter Referral Code'
                                                style={{
                                                    width:'60%',
                                                    fontSize:18,
    
                                                }} 
                                                onChangeText={v => setRCode(v.trim())}
                                                defaultValue={rCode}
                                            />
    
                                            <TouchableOpacity
                                                onPress={() => redeem()}
                                            >
                                                <Text
                                                style={{
                                                    fontSize:15,
                                                    color:colors.secondary
                                                }} 
                                                >
                                                    Confirm
                                                </Text>
                                            </TouchableOpacity>
    
                                        </View>
                                    }
                                    
                                </View>

                                <View
                                style={{
                                    alignItems:'center',
                                    marginBottom:60,
                                    width:'100%'

                                }}
                                >
                                    <Text
                                    style={{
                                        color:colors.secondary,
                                        marginHorizontal:30,
                                        lineHeight:30
                                    }}
                                        >YOUR REFERRAL CODE
                                    </Text>
                                    <View
                                    style={{
                                        flexDirection:'row',
                                    }}
                                    >
                                        <Text
                                        style={{
                                            color:colors.secondary,
                                            textAlign:'center',
                                            fontSize: 40,
                                        }}
                                        >
                                            {state.referal || 'NO CODE'}
                                        </Text>

                                        <TouchableOpacity
                                        style={{
                                            justifyContent:'flex-end',
                                            marginLeft:5
                                        }}

                                        onPress={() => {
                                            share(state.referal)
                                        }}
                                        >
                                            <Icon 
                                                name='copy'
                                                color={colors.secondary}
                                                size={20}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>

                                <View
                                style={{
                                    alignItems:'center',
                                    width:'100%'

                                }}
                                >
                                    <Text
                                    style={{
                                        fontSize:18,
                                        lineHeight:25,
                                        color:'gray',
                                        marginBottom:30,
                                        textAlign:'center',
                                        fontWeight:'300',
                                        width:'80%'
                                    }}
                                    >Get GHC1 everytime a friend signs up and makes a total transaction of GHC100
                                    </Text>

                                    <View
                                    style={{
                                        width:'90%'
                                    }}
                                    >
                                        <LoginPrimaryButton
                                            title='SHARE YOUR CODE'
                                            pressTo={() => {
                                                share(state.referal)
                                            }}
                                        />
                                    </View>
                                </View>

                                

                            </View>
                           
                        </>
                    );
                }
            }
        </ReferralConsumer>
    );
};

const styles = StyleSheet.create({
    container:{
        height:height,
        backgroundColor:'#fff',
        justifyContent:'flex-start',
        width:'100%'
    },
    scroll:{
        paddingHorizontal:50,
    },
    mainText:{
        paddingVertical:50,
        fontSize:20,
        fontWeight:'200',
    },
    code:{
        backgroundColor:'#eee',
        padding:20,
        borderRadius:5,
        marginVertical:25,
        flexDirection:'row',
        alignItems:'center',
    },
    modalContainer:{
        backgroundColor:'yellow',
    },
    modal:{
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#fff',
    },
    closeContainer:{
        position:'absolute',
        top:height / 20,
        left:0,
        margin:50,
    },
    modalContent:{
        marginVertical:height / 4,
        justifyContent:'center',
        alignItems:'center',
    },
    summaryCont:{
        marginVertical:20,
    },
    summayText:{
        fontWeight:'200',
        marginVertical:2,
    },
});

export default ReferalsComponent;



{/* <View style={styles.scroll}>
                                    <Text style={styles.mainText}>Share your referral code and get GHS1 every time a friend Sign's up and make a total transaction of GHS100.</Text>
                                    <View style={styles.code}>
                                        <IconButton onPress={()=>share(state.referal?.code)} name="share"/>
                                        <Text style={{fontWeight:'bold',marginLeft:10}}>{state.referal?.code || 'NO CODE'}</Text>
                                    </View>
                                    <View style={styles.summaryCont}>
                                        <Text style={styles.summayText}>Friends Registered: <Text>{state.summary?.count ?? 0}</Text></Text>
                                        <Text style={styles.summayText}>Rewards Earned: <Text>GH₵{state.summary?.total ?? 0}</Text></Text>
                                    </View>
                                    {state.referal?._id ? <LoginPrimaryButton
                                        loading={state.loading}
                                        title="Delete Code"
                                        pressTo={removeReferalCode}
                                    /> : <LoginPrimaryButton
                                        title="Generate Code"
                                        pressTo={codeGenerator}
                                    />}
                                </View> */}