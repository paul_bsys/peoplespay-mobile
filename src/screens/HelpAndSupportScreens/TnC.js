/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable eol-last */
/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import { View, Text, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import React from 'react'
import Header from '../../components/Header'
import colors from '../../constants/colors';
import { StyleSheet } from 'react-native';


const TnC = ({navigation}) => {
  return (
    <View
        style={{
            flex:1,
            backgroundColor:'white',
        }}
    >
        <Header text="" pressTo={()=>navigation.goBack()} />
        <View
            style={{
                width:'100%',
                height:'100%',
                flex:1,
                flexDirection:'column',
                alignItems:'center',
                marginVertical:10,
                paddingHorizontal:15
            }}
        >
            <Text
            style={{
                fontSize:20,
                color:colors.secondary,
            }}
            >
                Terms & Conditions
            </Text>
            <Text
                style={{
                    textAlign:'center',
                    fontSize:17,
                    marginTop:15,
                    color:'#888'
                }}
            >
                The following terms and conditions govern your use of Peoplespay. By using the Peoplespay platform, you have fully agreed to the terms applicable to its use.
            </Text>

              <ScrollView style={styles.scroll} contentContainerStyle={{ flexGrow: 1 }} contentInsetAdjustmentBehavior="automatic">
                <View style={styles.section}>
                    <Text style={styles.sTitle}>
                        HOW TO USE PEOPLESPAY
                    </Text>
                    
                    <Text style={styles.pgraph}>
                        You need to register in order to use Peoplespay. Registering on the platform does not take much time.
                        It is advised thar all users register on the platform to ensure smooth usage of the system.
                        We require certain personal information before you are onboard on the platform. You must provide your complete and accurate personal information and not information for any other person. By personal information we mean the following (full name, phone number, email address, mobile money, payment card information and government issued ID card where necessary)
                        Your PeoplesPay wallet is NOT a deposit receiving wallet but rather for the purpose of processing internal transaction reversals and internal transaction activities. Your mobile money wallet is not held by PeoplesPay but rather held by the respective Telecommunication companies and not Peoplespay.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.sTitle}>
                        FEES AND OTHER CHARGES
                    </Text>
                    <Text style={styles.pgraph}>
                        Using Peoplespay will attract a convenience fee charge which is subject to change. Other charges may come from your preferred channel (mobile network, bank or Card) for their service.
                    </Text>
                </View>
                <View style={styles.section}>
                    <Text style={styles.sTitle}>
                        Transactional and Daily Limits
                    </Text>
                    <View style={{marginTop:15}}>
                        <Text style={{marginBottom:10, fontSize:16, lineHeight:30}} >
                            Peoplespay has set a daily transaction limit of Ghs 5,000. It is possible your mobile money service provider would apply both a transactional and daily limit to your wallet. To increase or reduce these limits, you need to contact your service provider and not Peoplespay team.
                        </Text>

                        <View
                            style={{
                                flexDirection:'row',
                                width:'98%',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                            You must authorize your transactions with your secret PIN, which you created when registering with the mobile money service provider, or by such other method we may prescribe from time to time
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                                width:'98%',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                            You must save the mobile money number of the mobile money wallet from where the debit transaction is being initiated from
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                                width:'98%',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                            You are ONLY allowed to save one mobile money number per mobile network operator from which a debit can be initiated from.
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                                width:'98%',
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                            All mobile money numbers saved and from which debit transactions are initiated from MUST be the in the same name and mobile money numbers of the registered Peoplespay subscriber.
                            </Text>
                        </View>


                    </View>
                </View>
                <View style={styles.section}>
                    <Text style={styles.sTitle}>
                        SECURITY AND UNAUTHORIZES USE
                    </Text>
                    <Text style={styles.pgraph}>
                    The user selects confidential Personal Identification (PIN) during registration. This PIN is mandatory for the use of all Peoplespay features in such a manner that no transaction could be affected without entering the validating this PIN.
                    
                    You are responsible for keeping your PIN secret and for all transactions that take place on your wallet with your PIN, and you indemnify us against any claims made in respect of such transaction. Your PIN shall not be communicated to anyone, must be kept in a very confidential manner and should in no case be written on any document. The user must ensure this PIN is always composed out of sight of any individual.
                    
                    If at any time you believe or know that your cell phone or PIN has been stolen or compromised, kindly contact your service provider immediately.
                    
                    For the purpose of transactions authorization, the Peoplespay platform generates a One Time Passcode (OTP) which is sent to the mobile money number initiated the debit transaction. This OTP is randomly generated by a security algorithm and sent to the registered / saved phone number and it is not known to anyone. It is the responsibility of the receiver of the OTP initiated from the saved debit mobile money number to ensure he/she initiated the debit transaction that generated such OTP and should NEVER share the OTP with any other person whatsoever. 
                    
                    In a situation where the generated OTP is shared with any other person, Peoplespay will NOT be responsible for any claims of fraudulent debits to the mobile money wallets that may arise as a result, as this will be termed an act of negligence on the part of the Peoplespay subscriber.
                    
                    </Text>
                </View>
                  <View style={styles.section}>
                      <Text style={styles.sTitle}>
                          REFUNDS
                      </Text>

                      <Text style={styles.lTitle}>
                          Eligibility
                      </Text>


                      <Text style={styles.pgraph}>
                          Customers may be eligible for a refund if they have made a payment for aproduct or service through PeoplesPay and meet specific criteria outlinedbelow.
                      </Text>

                      <Text style={styles.lTitle}>
                          Criteria for Refunds
                      </Text>


                      <Text style={styles.pgraph}>
                          i. Defective or damaged goods upon delivery.
                          ii. Non-delivery of goods or services as promised by the merchant.
                          iii. Unauthorized transactions or fraudulent charges.
                          iv. Any other circumstances as specified by applicable laws and regulations.
                      </Text>

                      <Text style={styles.lTitle}>
                          Refund Process
                      </Text>


                      <Text style={styles.pgraph}>
                          i. Customers should contact our customer support within a specified timeframe (typically 7 days) from the date of the transaction to initiate a refundrequest.
                          ii. Our customer support team will guide customers through the necessarysteps to process the refund, which may include providing relevant transactiondetails, proof of purchase, and any supporting documentation.
                          iii. Upon successful verification, the refund will be processed within a reasonable time-frame, typically within 5 business days, using the original payment method.
                      </Text>
                  </View>
                  <View style={styles.section}>
                      <Text style={styles.sTitle}>
                          RETURNS
                      </Text>

                      <Text style={styles.lTitle}>
                          Return Policy
                      </Text>


                      <Text style={styles.pgraph}>
                          PeoplesPay is a payment service provider and does not directly handle
                          product returns. The return policy for products or services purchased throughour platform is determined by the respective merchants or sellers. Customersshould refer to the merchant's return policy for information on returns,
                          including eligibility, time-frame, and any associated fees.
                      </Text>

                      <Text style={styles.lTitle}>
                          Dispute Resolution
                      </Text>

                      <Text style={styles.pgraph}>
                          If customers encounter any issues with returns or face challenges in resolvingdisputes with the merchant, they can contact our customer support for guidance and assistance.
                      </Text>
                  </View>
                  <View style={styles.section}>
                      <Text style={styles.sTitle}>
                          CANCELLATIONS
                      </Text>

                      <Text style={styles.lTitle}>
                          Merchant Cancellation Policy
                      </Text>


                      <Text style={styles.pgraph}>
                          Cancellations of orders or services aresubject to the merchant's cancellation policy. Customers should refer to thespecific terms and conditions provided by the merchant for information oncancellations, including eligibility, time-frame, and any associated fees.
                      </Text>

                      <Text style={styles.lTitle}>
                          Refunds for Cancellations
                      </Text>

                      <Text style={styles.pgraph}>
                          If a merchant cancels an order or serviceand provides a refund, PeoplesPay will facilitate the refund process accordingto the merchant's instructions. Customers can contact our customer support
                          for any queries or assistance related to the cancellation and refund process.
                      </Text>
                  </View>
                  <View style={styles.section}>
                      <Text style={styles.sTitle}>
                          DELIVERY
                      </Text>

                      <Text style={styles.lTitle}>
                          Delivery Responsibility
                      </Text>


                      <Text style={styles.pgraph}>
                          PeoplesPay does not directly handle product
                          delivery. The responsibility for delivering products or services lies with therespective merchants or sellers. Customers should refer to the merchant's
                          delivery policy for information on shipping methods, timelines, and any
                          associated fees.
                      </Text>

                      <Text style={styles.lTitle}>
                          Delivery Issues
                      </Text>

                      <Text style={styles.pgraph}>
                          If customers experience any issues with the deliveryof
                          goods or services, such as delays, non-delivery, or damaged items, they
                          should contact the merchant directly to address the matter. However, customers can reach out to our customer support if they require assistanceor
                          have further concerns. Please note that PeoplesPay aims to ensure a smooth payment experiencefor
                          our customers, but we cannot be held responsible for the actions or policies
                          of individual merchants or sellers. We strongly recommend reviewing thespecific policies and terms provided by the merchants before making any
                          purchases. For any further inquiries or assistance, please don't hesitate to reach out toour customer support team.
                      </Text>
                  </View>
                <View style={styles.section}>
                    <Text style={styles.sTitle}>
                        CUSTOMER RESPONSIBILITIES
                    </Text>
                    <View style={{marginTop:15}}>
                        <View
                            style={{
                                flexDirection:'row',
                                width:'98%',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                                It is the responsibility of the wallet owner to ensure that he/she provides accurate personal information to Peoplespay. To this end, Peoplespay subscriber warrant that any information is true and correct and that he/she is obligated to provide any additional information that is required from time to time. Failing will result in suspension or closure of your account.
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                                The Peoplespay user will be responsible for payment of all applicable fees for any transaction effected using the platform whether these were made by you or someone else with or without your authority or knowledge.
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                                
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                                In the event of damage, loss or theft of the SIM, you are obliged to inform your service provider immediately of such damage, loss or theft. This will prevent any possible use of the service until the SIM card has been replaced. Any loss incurred during this period will be a personal liability for which Peoplespay shall not be responsible
                            </Text>
                        </View>

                        <View
                            style={{
                                flexDirection:'row',
                            }}
                        >
                            <Text
                                style={{
                                    marginRight:5,
                                    fontSize:18,
                                    fontWeight:'900'
                                }}
                            >
                                {'\u2022'}
                            </Text>
                            <Text
                                style={{
                                    fontSize:16,
                                    lineHeight:30,
                                    flexDirection:'row',
                                    flexWrap:'wrap',
                                    paddingHorizontal:15
                                }}
                            >
                                You must comply with any instructions that Peoplespay may issue from time to time about the use of the platform
                            </Text>
                        </View>

                        
                    </View>
                </View>
            </ScrollView>
        </View>

    </View>
  )
}

const styles = StyleSheet.create({
    section: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginTop: 20
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30,
        marginTop: 15,
        height: '100%'
    },
    sTitle: {
        color: colors.secondary,
        fontSize: 17,
    },
    ltitle: {
        fontSize: 17,
    },
    pgraph: {
        fontSize: 16,
        marginTop: 20,
        lineHeight: 30
    },
    pBullet: {
        marginBottom: 12,
        textAlign: 'center'
    },
    pBulletTxt: {
        fontSize: 16,
        lineHeight: 30,
    }
})

export default TnC