/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
/* eslint-disable eol-last */
/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import { View, Text,TouchableOpacity, Linking, StyleSheet } from 'react-native'
import React from 'react'
import Header from '../../components/Header'
import { Icon } from 'react-native-elements'
import colors from '../../constants/colors'
import { ScrollView } from 'react-native'
import {DrawerActions} from 'react-navigation-drawer';


const Legal = ({navigation}) => {
  return (
    <View style={styles.screen}>
        <Header 
            text="Legal" 
            pressTo={()=>navigation.navigate('Home')} 
            iconRight={true}
            iconRName='bars'
            iconRPress={()=>{
                navigation.dispatch(
                    DrawerActions.toggleDrawer(),
                );
            }}
        />
        <ScrollView style={{flex:1, backgroundColor:'white', width:'100%', flexDirection:'column', paddingTop:10}}>
            <TouchableOpacity
                style={styles.option}
                onPress={() =>
                    navigation.navigate('Privacy')
                }
            >
                <Icon
                    name="lock"
                    size={24}
                    color={colors.secondary}
                    type="font-awesome"
                />
                <Text style={styles.optionText}>
                    Privacy Policy
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={()=> navigation.navigate('TnC')}
                style={styles.option}
            >
                <Icon
                    name="file-pdf-o"
                    size={24}
                    color={colors.secondary}
                    type="font-awesome"
                />
                <Text style={styles.optionText}>
                    Terms and Conditions
                </Text>
            </TouchableOpacity>
        </ScrollView>
    </View>

  )
}


const styles = StyleSheet.create({
    screen: {
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        width:'100%'
    },
    option: {
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        padding:5,
        marginTop:15,
        marginHorizontal:10

    },
    optionText: {
        marginLeft:15,
        color:colors.secondary,
        fontWeight:'500',
        fontSize:18
    }
})

export default Legal