/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    ActivityIndicator,
    Alert,
    Image,
} from 'react-native';
import Colors from '../../constants/colors';
import BeneficiaryTiles from '../../components/BeneficiaryTiles';
import Icon from 'react-native-vector-icons/AntDesign';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Header from '../../components/Header';
import {BeneficiariesConsumer} from '../../states/beneficiaries.state';
import colors from '../../constants/colors';
import { TransactionContext } from '../../states/transactions.state';
import _BService from '../../services/beneficiary.service';
import HeaderText from '../../components/HeaderText';

const {height} = Dimensions.get('screen');



class BankBeneficiary extends Component {

    static contextType=TransactionContext;

    state={
        account:'',
        loading:false,
        issuers:[],
    };



    componentDidMount(){this.getBeneficiaries();}


    delete=async(id)=>{
        try {
            this.setState(
                {
                    loading:true,
                }
            );
            const response = await _BService.deleteBeneficiary(id);
            if (!response.success){
                throw Error(
                    response.message
                );
            }
            this.setState(
                {
                    loading:false,
                },()=>{
                    this.getBeneficiaries();
                    Alert.alert(
                        'Delete successful',
                        `${response.data.account_name} was successfully deleted from your saved accounts`
                    );
                }
            );
        } catch (err) {
            this.setState(
                {
                    loading:false,
                },()=>{
                    Alert.alert('Oops','We could not delete your account');
                }
            );
        }
    }



    render=()=>{
        return (
            <BeneficiariesConsumer>
                {({getBeneficiaries,state})=>{
                    this.getBeneficiaries = getBeneficiaries;
                    const beneficiaries = state.beneficiaries;
                    return (
                        <View style={styles.screen}>
                            <Header
                                text="Beneficiaries"
                                pressTo={()=>this.props.navigation.goBack()}
                            />
                            <ScrollView
                                contentContainerStyle={styles.scroll}
                                keyboardShouldPersistTaps="always"
                                keyboardDismissMode="on-drag">
                                    <HeaderText title1="Saved" title2="Beneficiaries"/>
                                <View style={styles.Body}>
                                
                                    {state.loading ? (
                                        <ActivityIndicator
                                            style={{
                                                margin: 10,
                                            }}
                                            size={'small'}
                                            color={colors.secondary}
                                        />
                                    ) : (beneficiaries.length <= 0 ? 
                                        <View style={{width:'100%', flexDirection:'column', alignItems:'center'}}>
                                            <Image source={require('../../assets/images/pplogo.png')} style={{width:300, height:400}}/>
                                            
                                        </View> :
                                        beneficiaries.map((ben,i)=>(
                                            
                                            <BeneficiaryTiles
                                                {...ben}
                                                key={i.toString()}
                                                showDelete
                                                delete={()=>Alert.alert(
                                                    'Confirm Delete',
                                                    'Do you want to delete this account',
                                                    [
                                                        {
                                                            text:'Yes,Delete',
                                                            onPress:()=>this.delete(ben._id),
                                                        },
                                                        {
                                                            text:'No,Cancel',
                                                            onPress:null,
                                                        },
                                                    ]
                                                )}
                                                title={ben.account_name}
                                                number={ben.account_number}
                                                issuer={ben.account_issuer_name}
                                                imageUri={{ uri:ben.account_issuer_image }}
                                            />
                                        ))
                                    )}
                                    <TouchableOpacity
                                    activeOpacity={0.7}
                                    // style={{width:'100%'}}
                                    onPress={() => {
                                        this.props.navigation.navigate('Beneficiary');
                                    }}>
                                    <View style={styles.ViewAllContainer}>
                                        <Icon
                                            name="pluscircle"
                                            size={25}
                                            color={Colors.secondary}
                                        />
                                        <Text style={styles.ViewAllText}>
                                            Create New Beneficiary
                                        </Text>
                                        
                                    </View>
                                </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </View>
                    );
                }}
            </BeneficiariesConsumer>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor:'white',
        paddingBottom:height / 16,
        width:'100%'
    },
    scroll: {
        flex:1,
        backgroundColor:'white',
        width:'100%'
    },
    Body: {
        width:'100%',
        // justifyContent: 'center',
        flexDirection:'column',
        // alignItems:'center',
    },
    HeaderTextMain: {
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingTop: 30,
        paddingBottom: 20,
        // flexDirection: 'row',
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.8,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 35,
        color: Colors.secondary,
        // textAlign: 'center',
        opacity: 0.8,
        bottom: 20,
        marginTop: 15,
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    ViewAllContainer: {
        // width:100,
        flexDirection: 'column',
        // justifyContent: 'flex-end',
        alignItems: 'center',
        // paddingHorizontal: 10,
        // marginBottom: 10,
        // width: '40%',
        alignSelf: 'center',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        paddingHorizontal: 15,
        paddingVertical: 10,
        color: Colors.tetiary,
        marginTop:10,

        fontSize: 12,
        fontFamily: 'Roboto-Medium',
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
    sample: {
        paddingHorizontal: 20,
    },
});
export default BankBeneficiary;
