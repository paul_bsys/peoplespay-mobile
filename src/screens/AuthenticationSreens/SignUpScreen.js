/* eslint-disable quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Dimensions,
    Alert,
    Platform,
    KeyboardAvoidingView,
    Linking,
    Modal
} from 'react-native';

import CheckBox from '@react-native-community/checkbox';
import Colors from '../../constants/colors';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import messages from '../../constants/errormessages';
import TnC from '../HelpAndSupportScreens/TnC';
// import Icon  from 'react-native-vector-icons/FontAwesome';
import Icon from "react-native-vector-icons/FontAwesome5"
import colors from '../../constants/colors';


// change cont declaration name and export default name
class SignUpScreen extends Component {

    state={
        fullname:'',
        phone: '',
        password: '',
        confirmPassword: '',
        ghCard: '',
        code:null,
        cchecked:false,
        schecked: false,
        openModal: false
    };

    Validate_email() {
        const {email} = this.state;
        let rjx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (rjx.test(email) === false) {
            alert('Please enter a valid email');
        }
    }

    Validate_phone() {
        const {phone} = this.state;
        let rjx = /^\+?\(?(([2][3][3])|[0])?\)?[-.\s]?\d{2}[-.\s]?\d{3}[-.\s]?\d{4}?$/;
        if (rjx.test(phone) === false) {
            alert('Please enter a phone number');
        }
    }

    Validate_fullname() {
        const {fullname} = this.state;
        let rjx = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s?)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
        if (rjx.test(fullname) === false) {
            alert('Please enter a vaild name');
        }
    }

    validateGhCard() {
        const {ghCard} = this.state;
        let rjx = /^(GHA)-([0-9]{9})-[0-9]$/;
        if(rjx.test(ghCard) === false) {
            alert('Invalid Ghana Card Number');
        }
    }

    render = () => {
        return (
            <ScrollView
                style={styles.scroll}
                keyboardShouldPersistTaps="always"
                keyboardDismissMode="on-drag"
            >
                <View style={styles.screen}>
                    {/* Header of the Screen */}
                    {/* <Header pressTo={() => props.navigation.goBack()} /> */}
                    <View style={styles.LogoContainer}>
                        <Image
                            style={styles.Logo}
                            source={require('../../assets/images/pp.png')}
                            resizeMode="contain"
                        />
                    </View>

                    {/* Screen Title */}
                    <View style={styles.HeaderTextMain}>
                        <Text style={styles.HeaderText}>Sign up</Text>
                        <Text style={styles.HeaderText1}>
                            create an account to get started
                        </Text>
                    </View>
                    {/* Screen Main Content */}
                    <KeyboardAvoidingView style={styles.Body}>
                        <View>
                            <TxtInput
                                placeholder="Full Name"
                                IconName="user"
                                IconType="feather"
                                onChangeText={v=>
                                    this.setState({
                                        fullname:v.trim(),
                                    })
                                }
                                onBlur={() => this.Validate_fullname()}
                            />
                            <TxtInput
                                placeholder="Ghana Card"
                                IconName="idcard"
                                IconType="antdesign"
                                onChangeText={v =>
                                    this.setState({ ghCard: v.trim() })
                                }
                                onBlur={()=> this.validateGhCard}
                            />
                            <TxtInput
                                maxLength={10}
                                placeholder="Phone number"
                                IconName="phone"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        phone:v.trim(),
                                    })
                                }
                                keyboardType="phone-pad"
                                onBlur={()=>this.Validate_phone()}
                            />
                            <TxtInput
                                password={true}
                                placeholder="Password"
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        password:v.trim(),
                                    })
                                }
                            />
                            <TxtInput
                                password={true}
                                placeholder="Confirm password "
                                IconName="lock"
                                IconType="feather"
                                onChangeText={v =>
                                    this.setState({
                                        confirmPassword:v.trim(),
                                    })
                                }
                            />
                        </View>

                        {/* Source Account Checkbox */}
                        {/* <View style={{flexDirection:'row', alignItems:'center', marginTop:'5%'}}>
                            <CheckBox
                                value={this.state.schecked}
                                onValueChange={v=>this.setState({schecked:v})}
                                
                            />
data
                            <Text style={{textAlign:'center', fontFamily:'Roboto-Bold', color:Colors.secondary, fontSize:14}}>
                                Same phone number for source account
                            </Text>
                        </View> */}

                        <View style={{paddingVertical:20}}>
                            <Text style={{ marginVertical:5 }}>If you have a referal code,please enter it.</Text>
                            <TxtInput
                                hideIcon
                                style={{marginVertical:10}}
                                placeholder="Code"
                                IconName="magnet"
                                IconType="fontawesome"
                                onChangeText={v=>this.setState({
                                    code:v.trim(),
                                    })
                                }
                            />
                        </View>

                        

                        {/* Terms and Conditons */}
                        <View style={styles.TnCsContainer}>
                            <View>
                                <Text style={styles.TnCsTextLight}>
                                    Check the box to proceed after reading our terms and conditions.
                                </Text>
                            </View>
                            <View
                                activeOpacity={0.7}
                                style={{flexDirection:'row',alignItems:'center',...Platform.select(
                                    {
                                        ios:{
                                            marginVertical:10
                                        }
                                    }
                                )}}
                            >
                                <CheckBox
                                    disabled={false}
                                    value={this.state.cchecked}
                                    onValueChange={v=>this.setState({cchecked:v})}
                                    style={{...Platform.select(
                                        {
                                            ios:{
                                                marginRight:10
                                            }
                                        }
                                    )}}
                                />
                                <TouchableOpacity
                                    onPress={()=> this.setState({openModal: true})}
                                    hitSlop={{
                                        top:10,
                                        bottom:10,
                                        left:10,
                                        right:10
                                    }}
                                >
                                    <Text style={styles.TnCsTextBold}>
                                        Terms and conditions
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.btn}>
                            <LoginPrimaryButton
                                title="Sign Up"
                                loading={this.state.loading}
                                pressTo={()=>{
                                    const data = this.state;
                                    let empty = null;
                                    Object.keys(data).forEach(key=>{
                                        if (data[key] === '') {
                                            empty = key;
                                        }
                                    });
                                    if (typeof empty === 'string') {
                                        return Alert.alert(
                                            'Oops!',
                                            'Please fill in all the required fields',
                                        );
                                    }
                                    if (!data.ghCard.match('^(GHA)-([0-9]{9})-[0-9]$')) {
                                        return Alert.alert(
                                            'Oops!',
                                            'Invalid Ghana Card Number'
                                        );
                                    }

                                    if (!data.password.match('^(?=.*[a-zA-Z0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,14}$')) {
                                        return Alert.alert('Oops!', 'Password must be between 8 and 14 characters and must contain a special character');
                                    }
                                    if (data.password !== data.confirmPassword) {
                                        return Alert.alert(
                                            'Oops!',
                                            'Passwords does match',
                                        );
                                    }
                                    if (!this.state.cchecked){
                                        return Alert.alert(
                                            'Oops',
                                            'Please agree to our terms and conditions before signup'
                                        );
                                    }
                                    const _data = {
                                        fullname: data.fullname,
                                        idNum: data.ghCard,
                                        password: data.password,
                                        phone: data.phone,
                                        code:data.code
                                    };
                                    this.props.navigation.navigate(
                                        'VerifyCustomer',
                                        {
                                            data:_data
                                        },
                                    );
                                }}
                            />
                        </View>
                    </KeyboardAvoidingView>

                    <View style={styles.textSigninContainer}>
                        <Text style={styles.textSignupLight}>Already have an account?</Text>
                        <TouchableOpacity
                            activeOpacity={0.7}
                            onPress={() =>
                                this.props.navigation.replace('Login')
                            }>
                            <Text style={styles.textSignup}> Login</Text>
                        </TouchableOpacity>
                    </View>
                </View>


                <Modal
                    animationType="slide"
                    onDismiss={()=>null}
                    presentationStyle="fullScreen"
                    visible={this.state.openModal}
                >
                    <View
                        style={styles.modal}

                    >
                        <TouchableOpacity
                            onPress={() => this.setState({openModal: false})}
                        >
                            <Icon
                                name='times'
                                size={30}
                                color={colors.secondary}
                            />
                        </TouchableOpacity>

                        <ScrollView
                            contentContainerStyle={{
                                width:'100%',
                                // height:'100%',
                                flex:1,
                                flexDirection:'column',
                                alignItems:'center',
                                marginVertical:10,
                                paddingHorizontal:15
                            }}
                        >
                            <Text
                            style={{
                                fontSize:20,
                                color:colors.secondary,
                            }}
                            >
                                Terms & Conditions
                            </Text>
                            <Text
                                style={{
                                    textAlign:'center',
                                    fontSize:17,
                                    marginTop:15,
                                    color:'#888'
                                }}
                            >
                                The following terms and conditions govern your use of Peoplespay. By using the Peoplespay platform, you have fully agreed to the terms applicable to its use.
                            </Text>

                            <View style={{marginTop:15, width:'100%'}}>
                                <View style={styles.section}>
                                    <Text style={styles.sTitle}>
                                        HOW TO USE PEOPLESPAY
                                    </Text>
                                    
                                    <Text style={styles.pgraph}>
                                        You need to register in order to use Peoplespay. Registering on the platform does not take much time.
                                        It is advised thar all users register on the platform to ensure smooth usage of the system.
                                        We require certain personal information before you are onboard on the platform. You must provide your complete and accurate personal information and not information for any other person. By personal information we mean the following (full name, phone number, email address, mobile money, payment card information and government issued ID card where necessary)
                                        Your PeoplesPay wallet is NOT a deposit receiving wallet but rather for the purpose of processing internal transaction reversals and internal transaction activities. Your mobile money wallet is not held by PeoplesPay but rather held by the respective Telecommunication companies and not Peoplespay.
                                    </Text>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.sTitle}>
                                        FEES AND OTHER CHARGES
                                    </Text>
                                    <Text style={styles.pgraph}>
                                        Using Peoplespay will attract a convenience fee charge which is subject to change. Other charges may come from your preferred channel (mobile network, bank or Card) for their service.
                                    </Text>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.sTitle}>
                                        Transactional and Daily Limits
                                    </Text>
                                    <View style={{marginTop:15}}>
                                        <Text style={{marginBottom:10, fontSize:16, lineHeight:30}} >
                                            Peoplespay has set a daily transaction limit of Ghs 5,000. It is possible your mobile money service provider would apply both a transactional and daily limit to your wallet. To increase or reduce these limits, you need to contact your service provider and not Peoplespay team.
                                        </Text>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                width:'98%',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                            You must authorize your transactions with your secret PIN, which you created when registering with the mobile money service provider, or by such other method we may prescribe from time to time
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                width:'98%',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                            You must save the mobile money number of the mobile money wallet from where the debit transaction is being initiated from
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                width:'98%',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                            You are ONLY allowed to save one mobile money number per mobile network operator from which a debit can be initiated from.
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                width:'98%',
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                            All mobile money numbers saved and from which debit transactions are initiated from MUST be the in the same name and mobile money numbers of the registered Peoplespay subscriber.
                                            </Text>
                                        </View>


                                    </View>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.sTitle}>
                                        SECURITY AND UNAUTHORIZES USE
                                    </Text>
                                    <Text style={styles.pgraph}>
                                    The user selects confidential Personal Identification (PIN) during registration. This PIN is mandatory for the use of all Peoplespay features in such a manner that no transaction could be affected without entering the validating this PIN.
                                    
                                    You are responsible for keeping your PIN secret and for all transactions that take place on your wallet with your PIN, and you indemnify us against any claims made in respect of such transaction. Your PIN shall not be communicated to anyone, must be kept in a very confidential manner and should in no case be written on any document. The user must ensure this PIN is always composed out of sight of any individual.
                                    
                                    If at any time you believe or know that your cell phone or PIN has been stolen or compromised, kindly contact your service provider immediately.
                                    
                                    For the purpose of transactions authorization, the Peoplespay platform generates a One Time Passcode (OTP) which is sent to the mobile money number initiated the debit transaction. This OTP is randomly generated by a security algorithm and sent to the registered / saved phone number and it is not known to anyone. It is the responsibility of the receiver of the OTP initiated from the saved debit mobile money number to ensure he/she initiated the debit transaction that generated such OTP and should NEVER share the OTP with any other person whatsoever. 
                                    
                                    In a situation where the generated OTP is shared with any other person, Peoplespay will NOT be responsible for any claims of fraudulent debits to the mobile money wallets that may arise as a result, as this will be termed an act of negligence on the part of the Peoplespay subscriber.
                                    
                                    </Text>
                                </View>
                                <View style={styles.section}>
                                    <Text style={styles.sTitle}>
                                        CUSTOMER RESPONSIBILITIES
                                    </Text>
                                    <View style={{marginTop:15}}>
                                        <View
                                            style={{
                                                flexDirection:'row',
                                                width:'98%',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                                It is the responsibility of the wallet owner to ensure that he/she provides accurate personal information to Peoplespay. To this end, Peoplespay subscriber warrant that any information is true and correct and that he/she is obligated to provide any additional information that is required from time to time. Failing will result in suspension or closure of your account.
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                                The Peoplespay user will be responsible for payment of all applicable fees for any transaction effected using the platform whether these were made by you or someone else with or without your authority or knowledge.
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                                
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                                In the event of damage, loss or theft of the SIM, you are obliged to inform your service provider immediately of such damage, loss or theft. This will prevent any possible use of the service until the SIM card has been replaced. Any loss incurred during this period will be a personal liability for which Peoplespay shall not be responsible
                                            </Text>
                                        </View>

                                        <View
                                            style={{
                                                flexDirection:'row',
                                            }}
                                        >
                                            <Text
                                                style={{
                                                    marginRight:5,
                                                    fontSize:18,
                                                    fontWeight:'900'
                                                }}
                                            >
                                                {'\u2022'}
                                            </Text>
                                            <Text
                                                style={{
                                                    fontSize:16,
                                                    lineHeight:30,
                                                    flexDirection:'row',
                                                    flexWrap:'wrap',
                                                    paddingHorizontal:15
                                                }}
                                            >
                                                You must comply with any instructions that Peoplespay may issue from time to time about the use of the platform
                                            </Text>
                                        </View>

                                        
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    
                    </View>
                    
                </Modal>

            </ScrollView>

        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
        marginVertical:40
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 30,
    },
    LogoContainer: {
        ...Platform.select({
            ios:{
                margin:30,
            },
        }),
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
    },
    Logo: {
        width: 150,
        height: 100,
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 35,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.7,
    },
    HeaderText1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: Colors.tetiary,
        // textAlign: 'center',
        opacity: 0.8,
        // bottom: 10,
    },
    Body: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'space-evenly',
        // paddingHorizontal: 30,
    },
    btn: {
        width: '100%',
        alignItems: 'center',
    },
    TnCsContainer: {
        flex: 1,
        // flexDirection: 'row',
        justifyContent: 'center',
        // paddingHorizontal: 30,
        paddingTop: 10,
        paddingBottom: 15,
        alignItems: 'center',
        alignContent: 'center',
    },

    TnCsTextLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 15,
        opacity: 0.85,
    },
    TnCsTextBold: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        textAlign: 'center',
        fontSize: 12,
    },

    textSigninContainer: {
        marginTop: Dimensions.get('window').height > 700 ? 100 : 28,
        // paddingVertical: 20,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    textSignupLight: {
        fontFamily: 'Roboto-Regular',
        color: Colors.tertiary,
        // textAlign: 'center',
        fontSize: 14,
        opacity: 0.8,
    },
    textSignup: {
        fontFamily: 'Roboto-Bold',
        color: Colors.secondary,
        // textAlign: 'center',
        fontSize: 14,
        // opacity: 0.85,
    },
    gradient: {
        flex: 1,
    },
    modal: {
        paddingTop: 50,
        paddingHorizontal: 20,
        flex: 1,
        // height:'100%'
    },
    section: {
        flexDirection:'column',
        alignItems:'flex-start',
        marginTop:20
    },
    sTitle: {
        color:colors.secondary,
        fontSize:17,
    },
    pgraph: {
        fontSize:16,
        marginTop:20,
        lineHeight:30,
        // width:'97.5%',
        // marginHorizontal:5,
        // textAlign:'center'
    },
    pBullet: {
        marginBottom:12,
        textAlign:'center'
    },
    pBulletTxt: {
        fontSize:16,
        lineHeight:30,
    }
});

export default SignUpScreen;
