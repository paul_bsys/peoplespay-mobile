/* eslint-disable quotes */
/* eslint-disable keyword-spacing */
/* eslint-disable no-unused-vars */
/* eslint-disable comma-dangle */
/* eslint-disable no-shadow */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable semi */
/* eslint-disable prettier/prettier */
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert, Platform, TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView } from 'react-native'
import React, { useState, useEffect, useContext } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import Header from '../../components/Header'
import TxtInput from '../../components/TxtInput'
import { SelectList } from 'react-native-dropdown-select-list';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import { height } from '../../components/TransactionView';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import colors from '../../constants/colors';
import { launchCamera } from 'react-native-image-picker';
import { PermissionsAndroid } from 'react-native';
import { AuthContext } from "../../states/auth.state";
import service from '../../services/users.service'

const VerifyCustomerID = ({ navigation }) => {

    const [idNumber, setIdNumber] = useState('')
    const [idType, setIdType] = useState('')
    const [image, setImage] = useState('https://unsplash.com/photos/2LowviVHZ-E')
    const [response, setResponse] = useState(undefined);
    const [base64, setBase64] = useState(undefined);
    const [loading, setLoading] = useState(false);

    const authContext = useContext(AuthContext)

    const user = authContext.state.user
    const logout = authContext.logout
    const idTypes = authContext.state.idTypes

    const options = {
        mediaType: 'photo',
        cameraType: 'front',
        includeBase64: true,
        quality: 0.5
    }

    useEffect(() => {
        if (user.isIDVerified) {
            Alert.alert('Hi', 'You have already updated and verified your ID, Thank you and continue using PeoplesPay')
            navigation.navigate('Home')
        } 
        // else {
        //     Alert.alert('Hey There,', 'In order to comply to regulatory requirements, you are mandated to upload your Id Card for further verification. Thank you')
        // }
    })

    const takePhoto = async () => {
        try {

            if (Platform.OS === 'android') {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: "App Camera Permission",
                        message: "PeoplesPay needs access to your camera ",
                        buttonNeutral: "Ask Me Later",
                        buttonNegative: "Cancel",
                        buttonPositive: "OK",
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    launchCamera(options, (res) => {
                        if (res.assets) {
                            setImage(res.assets[0].uri)
                            setBase64(res.assets[0].base64)
                        } else if (res.didCancel) {
                            setResponse('user cancelled')
                            Alert.alert('Oops!', response)
                        }
                        //  else if (res.errorCode){
                        //     setResponse(res.errorCode.toString())
                        // }
                        else {
                            setResponse(res?.errorMessage?.toString())
                            console.log('cameraErr', response);
                        }
                    })
                } else {
                    console.log("Camera permission denied");
                }
            } else {
                launchCamera(options, (res) => {
                    if (res.assets) {
                        setImage(res.assets[0].uri)
                        setBase64(res.assets[0].base64)
                    } else if (res.didCancel) {
                        setResponse('user cancelled')
                    }
                    //  else if (res.errorCode){
                    //     setResponse(res.errorCode.toString())
                    // }
                    else {
                        setResponse(res.errorMessage.toString())
                    }
                })
            }
        } catch (error) {
            Alert.alert('pher:', error);
        }
    }


    const verifyID = async () => {
        setLoading(true)
        if (base64 === undefined || base64 === '') {
            setLoading(false)
            Alert.alert('Sorry', 'No photo uploaded');
        } else {
            try {
                let data = {
                    customerId: user._id,
                    idNum: idNumber,
                    idType: 'GHCARD',
                    image: base64
                }

                let res = await service.verifyID(data);

                if (res.success) {
                    setLoading(false);
                    Alert.alert('', res.message);
                    setTimeout(logout(), 5000);
                } else {
                    setLoading(false);
                    Alert.alert('Oops!', res.message);
                }
            } catch (error) {
                setLoading(false)
                Alert.alert('Oops!', 'We encountered a problem verifying your ID');
                setTimeout(logout(), 5000);
            }
        }
    }

    return (
        <ScrollView
            style={styles.scroll}
            keyboardShouldPersistTaps="always"
            keyboardDismissMode="on-drag"
        >
            <KeyboardAvoidingView style={styles.Body}>
                <TouchableWithoutFeedback
                    onPress={() => Keyboard.dismiss()}
                >

                    <View style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', width: '100%', paddingHorizontal: 10 }}>
                        <Header
                            // text="Beneficiaries"
                            pressTo={() => {
                                if (user.isIDVerified) {
                                    navigation.navigate('Home')
                                } else {
                                    navigation.navigate('Home')
                                }
                            }}
                        />

                        <Text style={{ textAlign: 'center', color: colors.secondary, fontSize: 20 }}>Verify Your Identity</Text>
                        {/* <Text style={{textAlign:'center', marginTop:0, lineHeight:20}}>We must confirm your identity in order to be absolutely certain that you are You before we can complete a transaction. Since we provide financial services, we must abide by the KYC standards.</Text> */}
                        <Text style={{ textAlign: 'center', marginTop: 20, lineHeight: 20, color: colors.secondary, }}>Kindly take a photo of yourself and fill out the fields below.</Text>
                        {/* <Text>{response}</Text> */}
                        <View
                            style={{
                                width: '100%',
                                height: '30%',
                                marginTop: 20,
                                alignItems: 'center',
                                // backgroundColor:'skyblue',

                            }}
                        >
                            <View
                                style={{
                                    width: '60%',
                                    height: '80%',
                                    // backgroundColor:'skyblue',
                                    marginBottom: 10,
                                    borderRadius: 20,
                                    borderWidth: 2,
                                    borderColor: '#eee',
                                }}
                            >
                                <Image
                                    source={{ uri: image }}
                                    style={{
                                        width: '100%',
                                        height: '100%',
                                        // borderRadius:20,
                                        // backgroundColor:'skyblue',
                                    }}
                                />
                            </View>
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    borderBottomWidth: 2,
                                    borderBottomColor: colors.secondary,
                                    paddingBottom: 10,
                                }}

                                // onPress={() => console.log(idType , idNumber)}
                                onPress={() => takePhoto()}
                            >
                                <Text
                                    style={{
                                        fontSize: 16,
                                    }}
                                >
                                    Take a picturee

                                </Text>
                                <FontAwesome5Icon
                                    name="camera"
                                    size={20}
                                    color={colors.secondary}
                                    style={{
                                        marginLeft: 15,
                                    }}
                                />
                            </TouchableOpacity>

                        </View>

                        <View
                            style={{
                                paddingHorizontal: 20,
                                width: '100%',
                                marginTop: 20,
                            }}
                        >
                            {/* <View style={{ flexDirection: 'column' }}>
                                <Text style={styles.label}>ID TYPE</Text>

                                <View style={{ marginVertical: 20 }} >
                                    <SelectList
                                        data={idTypes}
                                        save="key"
                                        setSelected={val => setIdType(val)}
                                        placeholder="Select an Id Type"
                                        boxStyles={{
                                            borderColor: '#eee',
                                            paddingVertical: 20,
                                            paddingHorizontal: 15,
                                            borderWidth: 2,

                                        }}
                                        dropdownStyles={{
                                            borderColor: '#eee',
                                            paddingVertical: 10,
                                            paddingHorizontal: 5,
                                            borderWidth: 2,

                                        }}

                                    />
                                </View>
                            </View> */}

                            <View style={{ flexDirection: 'column', marginTop: 25 }}>
                                <Text style={styles.label}>GHANA CARD NUMBER</Text>
                                <TxtInput
                                    placeholder="Enter your Ghana Card number"
                                    hideIcon={true}
                                    onChangeText={v => {
                                            setIdNumber(v.trim())
                                        }
                                    }
                                />
                            </View>

                        </View>

                        <View
                            style={{
                                paddingHorizontal: 25,
                                marginTop: 20,
                                width: '80%',
                            }}
                        >
                            <LoginPrimaryButton
                                title="Verify ID"
                                pressTo={
                                    () => verifyID()
                                }
                                loading={loading}

                            />
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </ScrollView>
    )
}

export default VerifyCustomerID


const styles = StyleSheet.create({
    label: {
        textAlign: 'left',
        fontSize: 15,
        fontWeight: '400',
        color: 'grey',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        height: '100%'
        // paddingHorizontal: 30,
    },
    Body: {
        flex: 1,
        // alignItems: 'center',
        justifyContent: 'space-evenly',
        // paddingHorizontal: 30,
    },
})
