/* eslint-disable jsx-quotes */
/* eslint-disable comma-dangle */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    Alert,
    Dimensions,
    Image,
    Platform
} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import Icon from 'react-native-vector-icons/AntDesign';
import {AuthConsumer} from '../../states/auth.state';
import service from '../../services/wallet.service';
import colors from '../../constants/colors';
import HeaderText from '../../components/HeaderText';
import BeneficiaryTiles from '../../components/BeneficiaryTiles';
import { WalletConsumer } from '../../states/wallets.state';
const {height} = Dimensions.get('screen');
import {DrawerActions} from 'react-navigation-drawer';




// change cont declaration name and export default name
class CreateWalletScreen extends Component {

    subscription=null;
    isLoaded=false;

    state={
        loading: false,
        wallets:[],
        issuers:[],
    };

    componentDidMount=()=>{
        this.isLoaded = true;
        this.subscription = this.props.navigation.addListener('willFocus',this.getWallets);
    };


    componentWillUnmount(){
        this.subscription.remove();
        this.isLoaded = false;
    }


    deleteAccount=async(id)=>{
        try {
            this.isLoaded && this.setState({loading:true});
            const response = await service.deleteWallet(id);
            if (!response.success) {
                throw Error(response.message);
            }
            this.isLoaded && this.setState(
                {
                    loading:false,
                },()=>{
                    Alert.alert(
                        'Delete Successful',
                        'Your wallet was deleted successfully'
                    );
                    this.getWallets();
                }
            );
        } catch (err) {
            Alert.alert(
                'Delete Failed',
                'Sorry we could not delete account'
            );
            this.isLoaded && this.setState({loading: false});
        }
    };

    render=()=>{
        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state}) => {
                        this.user = state.user;
                        this.password = state.password;
                    }}
                </AuthConsumer>
                <Header 
                    text="Accounts" 
                    pressTo={()=>this.props.navigation.goBack()} 
                    iconRight={true}
                    iconRName='bars'
                    iconRPress={()=>{
                        this.props.navigation.dispatch(
                            DrawerActions.toggleDrawer(),
                        );
                    }}
                />
                <HeaderText title1="My Saved Accounts" />
                <View style={styles.Body}>
                    
                    <View style={styles.Container}>
                        <WalletConsumer>
                            {
                                ({state,getWallets})=>{
                                    this.getWallets = getWallets;
                                    return (
                                        <FlatList
                                            ListEmptyComponent={()=>{
                                                return state.loading ? (
                                                    <ActivityIndicator
                                                        color={colors.secondary}
                                                        size={'large'}
                                                    />
                                                ) : (<View style={{width:'100%', flexDirection:'column', alignItems:'center'}}>
                                                        <Image source={require('../../assets/images/pplogo.png')} style={{width:300, height:400}}/>
                                                        
                                                    </View>
                                                    
                                                );
                                            }}
                                            data={state.wallets}
                                            extraData={state}
                                            keyExtractor={(k,i)=>i.toString()}
                                            renderItem={({item})=>{
                                                return (
                                                    item && <BeneficiaryTiles
                                                        title={item.name ?? item.account_name}
                                                        number={item.account_number}
                                                        issuer={item.account_issuer_name}
                                                        imageUri={{ uri:item.account_issuer_image }}
                                                        showDelete
                                                        disabled
                                                        {...item}
                                                        delete={()=>{
                                                            Alert.alert(
                                                                'Delete Account',
                                                                `You have requested to delete your account with name ${item.account_name}`,
                                                                [
                                                                    {
                                                                        text: 'Yes, Delete',
                                                                        onPress: () =>
                                                                            this.deleteAccount(
                                                                                item._id,
                                                                            ),
                                                                    },
                                                                    {
                                                                        text: 'No, Cancel',
                                                                        onPress: null,
                                                                    },
                                                                ],
                                                            );
                                                        }}
                                                    />
                                                );
                                            }}
                                        />
                                    );
                                }
                            }
                        </WalletConsumer>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.7}
                        // style={{width:'100%'}}
                        onPress={() => {
                            this.props.navigation.navigate('ChooseWallet');
                        }}>
                        <View style={styles.ViewAllContainer}>
                            <Icon
                                name="pluscircle"
                                size={25}
                                color={Colors.secondary}
                            />
                            <Text style={styles.ViewAllText}>
                                Add My Account
                            </Text>
                            
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    HeaderTextMain: {
        color: Colors.secondary,
        textAlign: 'left',
        paddingHorizontal: 30,
        paddingVertical: 30,
        paddingBottom: 20,
    },
    HeaderText: {
        fontFamily: 'Roboto-ExtraBold',
        fontSize: 30,
        color: Colors.secondary,
        textAlign: 'left',
        opacity: 0.8,
    },
    Body: {
        width:'100%',
        // justifyContent: 'center',
        flexDirection:'column',
        alignItems:'center'
    },
    Container: {
        width:'100%',
        paddingBottom:'5%',
        marginBottom:25
        
    },
    mainContainer: {
        flex: 1,
        flexWrap: 'wrap',
        flexDirection: 'row',
    },
    ViewAllContainer: {
        // width:100,
        flexDirection: 'column',
        // justifyContent: 'flex-end',
        alignItems: 'center',
        // paddingHorizontal: 10,
        // marginBottom: 10,
        // width: '40%',
        alignSelf: 'center',
        marginHorizontal: 20,
    },
    ViewAllText: {
        textAlign: 'center',
        // backgroundColor: Colors.primary,,
        // paddingHorizontal: 5,
        // paddingVertical: 10,
        color: 'grey',
        fontSize: 12,
        fontFamily: 'Roboto-Medium',
        marginTop:10
    },
    cardItemlabel: {
        textAlign: 'center',
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
    },
    containerStyle: {
        padding: 10,
        borderWidth: 0,
        // marginBottom:5,
        marginTop: 10,
    },
    cardrow: {
        width: '100%',
        justifyContent: 'space-evenly',
        flexDirection: 'row',
    },
});

export default CreateWalletScreen;
