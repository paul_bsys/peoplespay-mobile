/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable no-return-assign */
/* eslint-disable semi */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {View, StyleSheet, ScrollView,Alert} from 'react-native';
// component imports
import HeaderTxt1 from '../../components/HeaderTxt1';
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Header from '../../components/Header';
import { AuthConsumer } from '../../states/auth.state';
import service from '../../services/users.service';


class EditProfileScreen extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading:false,
            id:'',
            oldpassword:'',
            newpassword:'',
            confirm:'',
            logout: null
        }
    }

    save=async()=>{
        try {
            const {
                id,
                oldpassword,
                newpassword,
                confirm
            } = this.state;
            const data = {
                id:id,
                oldpassword:oldpassword,
                newpassword:newpassword
            }

            Object.keys(data).forEach(key=>{
                if (key !== 'id'){
                    if (data[key] === ''){
                        throw Error(
                            `You have not provided value ${key}`
                        )
                    }
                }
            });

            if (newpassword === oldpassword){
                throw Error('New password cannot be the same as old password');
            }

            if (newpassword !== confirm){
                throw Error('Password does not match');
            }

            this.setState({loading:true});
            const response = await service.updatePassword(data);
            if (!response.success){
                throw Error(response.message);
            }

            this.setState(
                {
                    loading:false
                }, ()=>{
                    Alert.alert(
                        'Update Password',
                        response.message
                    )
                    this.state.logout()
                }
            )


        } catch (err) {
            this.setState(
                {
                    loading:false
                }, ()=>{
                    Alert.alert(
                        'Oops!',
                        err.message
                    )
                }
            )
        }
    }


    render(){
        return (
            <View style={styles.scroll}>

                <AuthConsumer>
                    {
                        ({state, logout})=>{
                            this.state.id = state.user._id;
                            this.state.logout=logout
                        }
                    }
                </AuthConsumer>
                <Header pressTo={() => { global.currentScreenIndex = 0;
                                this.props.navigation.goBack();}} />

                <HeaderTxt1
                    txt1="Change"
                    txt2=" Password"
                />
                <View style={styles.screen}>
                    <View style={styles.textFieldContainer}>
                        {/* Old Password */}
                        <TxtInput
                            placeholder="Enter Old Password"
                            password={true}
                            onChangeText={v=>this.state.oldpassword = v.trim()}
                        />

                        {/* New Password */}
                        <TxtInput
                            placeholder="Enter New Password"
                            password={true}
                            onChangeText={v=>this.state.newpassword = v.trim()}
                        />

                        {/* Retype Password */}
                        <TxtInput
                            placeholder="Confirm New Password"
                            password={true}
                            onChangeText={v=>this.state.confirm = v.trim()}
                        />
                    </View>

                    {/* button prop */}
                    <View
                        style={{width:'100%', paddingHorizontal:60, marginVertical:25}}
                    >
                        <LoginPrimaryButton
                            loading={this.state.loading}
                            title="SAVE CHANGES"
                            pressTo={this.save.bind(this)}
                        />
                    </View>
                    
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        alignItems:'center',
    },
    screen: {
        // flex: 1,
        alignItems: 'center',
        paddingTop: 20,
        width:'100%',
        // justifyContent:'space-evenly'

    },
    textFieldContainer: {
        // paddingBottom: 30,
        paddingHorizontal: 30,
        // height:'50%',
        // flex:1,
        width:'100%',
    },
});

export default EditProfileScreen;
