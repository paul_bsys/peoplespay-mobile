/* eslint-disable semi */
/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable jsx-quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, StyleSheet, ScrollView, Alert, Text} from 'react-native';
// component imports
import TxtInput from '../../components/TxtInput';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import Header from '../../components/Header';
// import HeaderTxt1 from '../../components/HeaderTxt1';
import {AuthConsumer, AuthContext} from '../../states/auth.state';
import service from '../../services/users.service';
import Icon  from 'react-native-vector-icons/FontAwesome';
import colors from '../../constants/colors';
import {DrawerActions} from 'react-navigation-drawer';
import RadioGroup from 'react-native-radio-buttons-group'
import { SelectList } from "react-native-dropdown-select-list";



class EditProfileScreen extends Component {
    static contextType=AuthContext;

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            loading: false,
            mailErr: '',
            gender:'',
            email:'',
            phone:'',
            fullname: '',
            idNumber:'',
            idType:'',
            address:'',
            digitalAddress:'',
            radioButtons: [
                {
                    id: '1',
                    label: 'Male',
                    value: 'male'
                },
                {
                    id: '2',
                    label: 'Female',
                    value: 'female'
                }
            ],
            idTypes: [
                {
                    key: '1',
                    value: 'Ghana Card'
                },
                {
                    key: '2',
                    value: "Driver's License"
                }
                ,
                {
                    key: '3',
                    value: "Voter's ID"
                }
            ],
            selectedID: {}
        };
    }

    

    onPressRadioButton =(radioButtonsArray) => {
        if (this.state.gender !== undefined && this.state.gender !== '') {

            for (let i=0; i<this.state.radioButtons.length; i++){
                if (this.state.radioButtons[i].value === this.state.gender){
                    delete this.state.radioButtons[i].selected
                    
                }
            }
        }
        

        this.setState({
            radioButtons : radioButtonsArray
        })
        for (let i=0; i<this.state.radioButtons.length; i++){
            if (this.state.radioButtons[i].selected === true){
                this.setState({
                    gender: this.state.radioButtons[i].value
                } //, () => console.log(this.state.gender)
                )
            }
        }
        
    }

    defaultGender = (gen) => {
        for (let i=0; i<this.state.radioButtons.length; i++){
            if (this.state.radioButtons[i].value === gen){
                // let rb = this.state.radioButtons[i].selected
                // console.log('rb   ',this.state.radioButtons[i]);
                this.state.radioButtons[i].selected = true
                // break;
                
            }
        }
    }

    defaultIdType = (val) => {
        for (let i=0; i<this.state.idTypes.length; i++){
            if (this.state.idTypes[i].value === val){

                // this.state.radioButtons[i].selected = true
                this.setState({selectedID: this.state.idTypes[i]})
                
            }
        }
    }


    checkValues = (u) => {
        const _data = {};
        if (this.state.gender === '') {
            _data.gender = u.gender
        } else {
            _data.gender = this.state.gender
        }
        if (this.state.address === '') {
            _data.address = u.address
        }else {
            _data.address = this.state.address
        }
        if (this.state.digitalAddress === '') {
            _data.digitalAddress = u.digitalAddress
        }else {
            _data.digitalAddress = this.state.digitalAddress
        }
        if (this.state.email === '' || !this.state.email.match(/^[\w]+@([\w]+\.)+[\w]{2,4}$/)) {
            _data.email = u.email
        }else {
            _data.email = this.state.email
        }
        if (this.state.fullname === '') {
            _data.fullname = u.fullname
        }else {
            _data.fullname = this.state.fullname
        }
        if (this.state.phone === '') {
            _data.phone = u.phone
        }else {
            _data.phone = this.state.phone
        }
        if (this.state.idNumber === '') {
            _data.idNumber = u.idNumber
        }else {
            _data.idNumber = this.state.idNumber
        }
        if (this.state.idType === '') {
            _data.idType = u.idType
        }else {
            _data.idType = this.state.idType
        }

        return _data;
    }

    updateUser = async (values) => {
        try {
            this.setState({loading: true});
            const response = await service.updateCustomer({
                id: this.state.id,
                data: values,
            });

            if (!response.success) {
                throw Error(response.message);
            }
            this.setState(
                {
                    loading: false,
                },
                () => {
                    Alert.alert('Account Update', response.message);
                    this.props.navigation.navigate('Home');
                },
            );
            this.updateState(
                {
                    user: response.data,
                }
            );
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                () => {
                    Alert.alert('Oops!', err.message);
                },
            );
        }
    };

    renderIDName = async (type) => {
        let _name = ''
        switch (type) {
            case 'VOTER':
                _name = 'VOTER ID';
                break;
            case 'PASSPORT':
                _name = 'PASSPORT';
                break;
            default:
                _name = 'GHANA CARD';
                break;
        }
        return _name;
    }

    render() {
        // const idTypes = [
        //     {key:'1', value:'Ghana Card'},
        //     {key:'2', value:"Driver's License"},
        //     {key:'3', value:"Voter's ID"},
        // ]
        return (
            <AuthConsumer>
                {({updateState, state}) => {
                    this.updateState = updateState;
                    const {user} = state;
                    this.state.id = user._id;
                    if (user.gender !== undefined && this.state.gender === '') {
                        this.state.gender = user.gender;
                        this.defaultGender(this.state.gender);
                    }

                    if (user.idType !== undefined && this.state.idType === '') {
                        this.state.idType = user.idType;
                        this.defaultIdType(this.state.idType)
                    }
                    
                    return (
                        <ScrollView style={styles.scroll}>
                            {/* Header Component */}

                            <Header
                                pressTo={() => { global.currentScreenIndex = 0;
                                    this.props.navigation.goBack();}}
                                iconRight={true}
                                iconRName= 'bars'
                                iconRPress={()=>{
                                    this.props.navigation.dispatch(
                                        DrawerActions.toggleDrawer(),
                                    );
                                }}
                            />
    
                            {/* <HeaderTxt1
                                txt1="Edit"
                                txt2=" Profile"
                                pressTo={() => this.props.navigation.goBack()}
                            /> */}
                            <View style={styles.screen}>
                                <View style={{padding:20, borderRadius:50, backgroundColor:colors.secondary}}>
                                    <Icon 
                                        name='user-circle-o'
                                        size={60}
                                        color='#fff'
                                    />
                                </View>
                                <View style={styles.textFieldContainer}>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>Your Name</Text>
                                        <TxtInput
                                            placeholder="Full Name"
                                            onChangeText={v => {
                                                this.setState({
                                                    fullname: v.trim()
                                                })
                                                // console.log(this.state.fullname);
                                            }
                                            }
                                            defaultValue={user.fullname}
                                            hideIcon={true}
                                        />
                                    </View>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>Your Email</Text>
                                        <TxtInput
                                            placeholder="Email"
                                            // IconName="mail"
                                            // IconType="feather"
                                            onChangeText={v => {
                                                // (this.state.email = v.trim())
                                                    this.setState({
                                                        email: v.trim()
                                                    })
                                                }
                                            }
                                            keyboardType="email-address"
                                            defaultValue={user.email}
                                            hideIcon={true}
                                        />
                                        {this.state.mailErr !== '' && <Text style={{fontSize:10,color:colors.secondary, marginTop:3,}}>{this.state.mailErr}</Text>}
                                        
                                    </View>

                                    <View style={{flexDirection:'column', marginVertical:20}}>
                                        <Text style={styles.label}>Gender</Text>
                                        {/* <TxtInput
                                            defaultValue={user.gender}
                                            hideIcon={true}
                                        /> */}

                                        <RadioGroup
                                            radioButtons={this.state.radioButtons}
                                            onPress={this.onPressRadioButton}
                                            layout={'row'}
                                        />
                                    </View>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>Phone Number</Text>
                                        <TxtInput
                                            placeholder="Phone number"
                                            defaultValue={user.phone}
                                            hideIcon={true}
                                            keyboardType='numeric'
                                            onChangeText={v => {
                                                // (this.state.phone = v.trim())
                                                    this.setState({
                                                        phone: v.trim()
                                                    })
                                                // console.log(this.state.phone);
                                                }
                                            }
                                        />
                                    </View>

                                    {/* <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>ID TYPE</Text>
                                        
                                        <View style={{marginVertical:20}} >
                                            <SelectList
                                                data={this.state.idTypes}
                                                save='value'
                                                setSelected={val => this.setState({idType: val}, () => console.log(this.state.idType))}
                                                placeholder='Select an Id Type'
                                                boxStyles = {{
                                                    borderColor: '#eee',
                                                    paddingVertical:10,
                                                    paddingHorizontal: 5,
                                                    borderWidth:2,

                                                }}
                                                dropdownStyles = {{
                                                    borderColor: '#eee',
                                                    paddingVertical:10,
                                                    paddingHorizontal: 5,
                                                    borderWidth:2,

                                                }}
                                                defaultOption={this.state.selectedID}

                                            />
                                        </View>
                                    </View> */}

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>ID TYPE</Text>
                                        <TxtInput
                                            placeholder='Enter your ID type'
                                            defaultValue={user.idType}
                                            hideIcon={true}
                                            editable={false}
                                            onChangeText={v => {
                                                // (this.state.ghanaCard = v.trim())
                                                    this.setState({
                                                        idNumber: v.trim()
                                                    })
                                                // console.log(this.state.idNumber);
                                                }
                                            }
                                        />
                                    </View>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>ID NUMBER</Text>
                                        <TxtInput
                                            placeholder='Enter your ID number'
                                            defaultValue={user.idNumber}
                                            hideIcon={true}
                                            editable={false}
                                            onChangeText={v => {
                                                // (this.state.ghanaCard = v.trim())
                                                this.setState({
                                                    idNumber: v.trim()
                                                })
                                                // console.log(this.state.idNumber);
                                            }
                                            }
                                        />
                                    </View>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>ADDRESS</Text>
                                        <TxtInput
                                            placeholder='Enter your address'
                                            defaultValue={user.address}
                                            hideIcon={true}
                                            onChangeText={v => {
                                                // (this.state.address = v.trim())
                                                this.setState({
                                                    address: v.trim()
                                                })
                                                // console.log(this.state.address);
                                            }
                                            }
                                        />
                                    </View>

                                    <View style={{flexDirection:'column', marginTop:20}}>
                                        <Text style={styles.label}>DIGITAL ADDRESS</Text>
                                        <TxtInput
                                            defaultValue={user.digitalAddress}
                                            placeholder='Enter your digital address'
                                            hideIcon={true}
                                            onChangeText={v => {
                                                // (this.state.digitalAddress = v.trim())
                                                this.setState({
                                                    digitalAddress: v.trim()
                                                })
                                                // console.log(this.state.digitalAddress);
                                            }
                                            }
                                        />
                                    </View>
                                    
                                    
                                </View>

                                {/* button prop */}
                                <View
                                    style={{width:'100%', paddingHorizontal:50, marginBottom:25}}
                                >
                                    <LoginPrimaryButton
                                        loading={this.state.loading}
                                        title="SAVE CHANGES"
                                        pressTo={() => {
                                            // if (!this.state.email.match(/^[\w]+@([\w]+\.)+[\w]{2,4}$/)) {
                                            //     return (
                                            //         this.setState({mailErr: 'Inavlid Email Format!'}),
                                            //         console.log("here")
                                            //     );
                                            // }
                                            let dt = this.checkValues(user);
                                            this.updateUser(dt);
                                        }}
                                    />
                                </View>
                                
                            </View>
                        </ScrollView>
                    );
                }}
            </AuthConsumer>
        );
    }
}

const styles = StyleSheet.create({
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    screen: {
        flex: 1,
        alignItems: 'center',
    },
    textFieldContainer: {
        // paddingBottom: 30,
        // paddingHorizontal: 10,
        flexDirection:'column',
        justifyContent:'space-evenly',
        marginVertical:30,
    },
    label: {
        textAlign:'left',
        fontSize:15,
        fontWeight:'400',
        marginLeft:7,
        color:'grey'
    }
});

export default EditProfileScreen;
