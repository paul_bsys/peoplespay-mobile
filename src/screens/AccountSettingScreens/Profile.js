/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable eol-last */
/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import { View, Text, ScrollView, StyleSheet } from 'react-native';
import React, { useState} from 'react';
import Header from '../../components/Header';
import { AuthConsumer } from '../../states/auth.state';
import colors from '../../constants/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import { ReferralConsumer } from "../../states/referrals.state";
import { TouchableOpacity } from 'react-native-gesture-handler';

const Profile = ({ navigation }) => {
    const [rCode, setRCode] = useState('')

  return (
    <AuthConsumer>
        {({state}) => {
            const {user} = state;
            return (
                <ReferralConsumer>
                    {({state}) => {
                    setRCode(state.referal || 'N/A')
                    return (

                        <View
                        style={{
                            flex:1,
                            backgroundColor:'white'
                            
                        }}
                    >
                        <Header text="Profile" pressTo={()=>navigation.goBack()} />
    
                        <ScrollView
                            contentContainerStyle={{
                                // justifyContent: 'center',
                                alignItems: 'center',
                                // marginBottom:5,
                                // marginTop:-20,
                                // borderRadius:100,
                                // backgroundColor:'yellow',
                                // padding:20,
                                // width:'100%',
                                flex:1
                            }}
                        >
                            
                            <View
                                style={{
                                    flex:1,
                                    // paddingHorizontal:10,
                                    width:'95%',
                                    // height:'100%',
                                    // marginTop:10,
                                    alignItems:'center',
                                    // backgroundColor:'blue'
                                }}
                            >
                                    <View style={{padding:25, marginBottom:15, borderRadius:45, backgroundColor:colors.secondary, width:'27%', alignItems:'center'}}>
                                        <Icon 
                                            name="user-circle-o"
                                            size={50}
                                            color="#fff"
                                        />

                                    </View>

                                    <View>
                                        <TouchableOpacity
                                            onPress={()=> navigation.navigate('EditProfile')}
                                            style={{flexDirection:'row', borderBottomWidth:2, borderBottomColor:colors.secondary,alignItems:'center',}}
                                        >
                                            <Text
                                                style={{color:colors.secondary, fontSize:17, marginRight:10}}
                                            >Edit Profile</Text>
                                            <Icon 
                                                name="pencil"
                                                size={17}
                                                color={colors.secondary}
                                            />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{width:'100%',  paddingHorizontal:25,marginTop:20,}}>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                Full Name
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.fullname || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                YOUR EMAIL
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.email || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                GENDER
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.gender || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                PHONE NUMBER
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.phone || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                ID TYPE
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.idType || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                ID NUMBER
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.idNumber || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                ADDRESS
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.address || 'N/A'}
                                            </Text>
                                        </View>
    
                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                DIGITAL ADDRESS
                                            </Text>
                                            <Text style={styles.txt}>
                                                {user.digitalAddress || 'N/A'}
                                            </Text>
                                        </View>

                                        <View style={styles.tBox}>
                                            <Text style={styles.label}>
                                                REFERRAL CODE
                                            </Text>
                                            <Text style={styles.txt}>
                                                {rCode}
                                            </Text>
                                        </View>
    
                                    </View>
    
                                    {/* <View
                                        style={{width:'100%', marginVertical:30, paddingHorizontal:40}}
                                    >
                                        <LoginPrimaryButton
                                            title="Edit Profile"
                                            pressTo={() => navigation.navigate('EditProfile')}
                                        />
                                    </View> */}
                                    
                            </View>
                            
                            
                        </ScrollView>
    
                    </View>

                    );

                    }}

               
                </ReferralConsumer>

            );
        }}
        
    </AuthConsumer>
    
  );
};

export default Profile;



const styles = StyleSheet.create({
    tBox: {
        // paddingVertical:10,
        marginBottom:25,
        width:'100%'
    },
    label: {
        fontSize:12,
        color:'lightgray'
    },
    txt: {
        fontSize:18,
        color:'gray',
        marginTop:5
    }
});