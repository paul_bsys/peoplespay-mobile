/* eslint-disable react/no-did-mount-set-state */
/* eslint-disable space-unary-ops */
/* eslint-disable quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable jsx-quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Header from '../../components/Header';
import {ScrollView} from 'react-native-gesture-handler';
import TxtInput from '../../components/TxtInput';
import HeaderText from '../../components/HeaderText';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import {AuthConsumer} from '../../states/auth.state';
import {CustomExample} from '../../components/customPicker';
import LazyContactPicker from '../../components/contacts-picker.component';
import { TransactionContext } from '../../states/transactions.state';
import {DrawerActions} from 'react-navigation-drawer';
import colors from '../../constants/colors';





// change cont declaration name and export default name
class BankAccount extends Component {

    static contextType=TransactionContext;

    state={
        customerId: '',
        amount: '',
        amountValid: true,
        recipient_account_number: '',
        recipient_account_numberValid: true,
        recipient_account_issuer: '',
        recipient_account_issuerValid: true,
        recipient_account_type: '',
        recipient_account_typeValid: true,
        description:'',
        descriptionValid: true,
        recipient_type: '',
        openModal:false,
        loading: false,
        numErr:'',
    };

    validate=(v,type)=>{
        const {recipient_account_type} = this.state;
        // Alert.alert('pree');
        if (recipient_account_type === 'momo' && type === 'accountnumber') {
            if (/^[0]?\d{9}$/.test(v) && v.length === 10) {
                // Alert.alert('match');
                this.setState({
                    recipient_account_numberValid: true,
                    recipient_account_number: v.trim(),
                    loading: false,
                    numErr:''
                });
                
            } else {
                // Alert.alert("not match");
                this.setState({
                    recipient_account_numberValid: false,
                    loading: true,
                    numErr:'Invalid Number Format. Eg. 024XXXXXXX'
                });
            }
            
        }
        // Alert.alert('post');


        if (recipient_account_type === 'bank' && type === 'accountnumber') {
            if (/^\d{10,17}$/.test(v)) {

                this.setState({
                    recipient_account_numberValid: true,
                    recipient_account_number: v.trim(),
                    loading: false,
                    numErr:''


                });
                
            } else {
                this.setState({
                    recipient_account_numberValid: false,
                    loading: true,
                    numErr:'Invalid Number Format. Eg. 0100XXXXXXX'




                });
            }
        }

        if (type === 'accountname') {
            if (v !== '' || /^[a-zA-Z]+/.test(v)) {
                this.setState({
                    recipient_account_nameValid: true,
                    loading: false

                });
                
            } else {
                this.setState({
                    recipient_account_nameValid: false,
                    loading: true

                });
            }
        }

        if (type === 'desc') {
            if (/^[\da-zA-Z]+/.test(v)) {
                this.setState({
                    descriptionValid: true,
                    description: v.trim(),
                    loading: false

                });
            } else {
                
                this.setState({
                    descriptionValid: false,
                    loading: true

                });
            }
        }

        if (type === 'amount') {
            if (v !== '' || /^[0-9]+/.test(v)) {
                
                this.setState({
                    amount_Valid: true,
                    amount: v.trim(),
                    loading: false

                });
            } else {
                this.setState({
                    amount_Valid: false,
                    loading: true

                });
            }
        }

        if (type === 'issuer') {
            if (v !== '') {
                this.setState({
                    recipent_account_issuerValid: true,
                    loading: false

                });
            } else {
                
                this.setState({
                    recipient_account_issuerValid: false,
                    loading: true

                });
            }
        }
    };


    componentDidMount=()=>{
        const {transaction} = this.context.state;
        this.setState({
            recipient_account_type: transaction.recipient_account_type
        });
    };



    render=()=>{

        const {state,pushTransaction} = this.context;
        const {transaction} = state;

        return (
            <View style={styles.screen}>
                <AuthConsumer>
                    {({state})=>{
                        const {user} = state;
                        this.state.customerId = user._id;
                    }}
                </AuthConsumer>
                <Header 
                    text="Transaction Details" 
                    pressTo={()=>this.props.navigation.goBack()} 
                    iconRight={true}
                    iconRName='bars'
                    iconRPress={()=>{
                        this.props.navigation.dispatch(
                            DrawerActions.toggleDrawer(),
                        );
                    }}
                />
                <ScrollView
                    style={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText
                        title1="Send money"
                        title2={`to ${transaction.recipient_account_issuer_name} Account`}
                    />
                    <View style={styles.Body}>
                        <View style={styles.TxtInputContainer}>
                            {transaction.recipient_type === 'onetime' && (
                                <View>
                                <TxtInput
                                    style={
                                        !this.state
                                            .recipient_account_numberValid
                                            ? {
                                                  borderColor: 'red',
                                                  borderWidth: 1,
                                              }
                                            : null
                                    }
                                    placeholder={
                                        this.state.recipient_account_type ===
                                        'momo'
                                            ? 'Phone Number'
                                            : 'Account number'
                                    }
                                    keyboardType="numeric"
                                    IconName="address-book"
                                    IconType="font-awesome"

                                    contact={true}
                                    defaultValue={this.state.recipient_account_number}
                                    onChangeText={v=>{
                                        this.validate(v,'accountnumber');
                                    }}
                                    onContact={()=>this.setState(
                                        {
                                            openModal:true
                                        }
                                    )}
                                />
                                {this.state.numErr !== '' && <Text style={{color:colors.secondary, fontSize:12, marginTop:5}}>{this.state.numErr}</Text>}
                                </View>
                            )}
                            <TxtInput
                                style={
                                    !this.state.amountValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 1,
                                          }
                                        : null
                                }
                                keyboardType="number-pad"
                                placeholder="Amount"
                                IconName="money"
                                IconType="font-awesome"
                                onChangeText={v => {
                                    // this.setState({amount:v.trim()});
                                    this.validate(v, 'amount');
                                }}
                            />
                            <TxtInput
                                style={
                                    !this.state.descriptionValid
                                        ? {
                                              borderColor: 'red',
                                              borderWidth: 1,
                                          }
                                        : null
                                }
                                placeholder="Description"
                                IconName="description"
                                IconType="material"
                                onChangeText={v => {
                                    // this.setState({description: v});
                                    this.validate(v, 'desc');
                                }}
                            />
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                title="Proceed"
                                loading={this.state.loading}
                                pressTo={()=>{
                                    try {
                                        if (this.state.amount === '' || this.state.recipient_account_number === '' || this.state.description === ''){
                                             Alert.alert('OOPS!', 'Kindly Fill All Fields');
                                             this.setState({loading: true});
                                            return;
                                        }
                                        let data = {};
                                        switch (transaction.recipient_type) {
                                            case 'beneficiary':
                                                data = {
                                                    customerId:this.state.customerId,
                                                    amount:this.state.amount,
                                                    description:this.state.description,
                                                };
                                                break;
                                            default:
                                                data = {
                                                    customerId:this.state.customerId,
                                                    amount:this.state.amount,
                                                    description:this.state.description,
                                                    recipient_account_number:this.state.recipient_account_number || ''
                                                };
                                                break;
                                        }
                                        Object.values(data).forEach(v=>{
                                            if (!v || v === ''){
                                                throw Error(
                                                    'INVALID DATA'
                                                );
                                            }
                                        });
                                        pushTransaction(data,()=>{
                                            this.props.navigation.navigate(
                                                'ChooseSource',
                                                {
                                                    page:'SenderDetailsMoMo',
                                                    data:{ page:'BankRecipientDetails' },
                                                }
                                            );
                                            }
                                        );
                                    } catch (err) {

                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
                <LazyContactPicker
                    openModal={this.state.openModal}
                    getContact={(v)=>{
                        try {
                            this.setState(
                                {
                                    openModal:false,
                                    recipient_account_number:v.trim()
                                },()=>{
                                    this.validate(v,'accountnumber');
                                }
                            );
                        } catch (err) {}
                    }}
                    closeModal={
                        ()=>this.setState(
                            {
                                openModal:false
                            }
                        )
                    }
                />
            </View>
        );
    };
}
const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
    },
    Body: {
        justifyContent: 'center',
        flex: 1,
    },
    heading: {
        textAlign: 'left',
        paddingVertical:5,
        // marginBottom: 10,
        // borderBottomWidth: 0.8,
        // borderColor: Colors.primary,
        color: 'white',
        opacity: 1,
        fontFamily: 'Roboto-Bold',
        fontSize:18,
        textAlign: 'center',
        textAlignVertical: 'center',
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
});

export default BankAccount;
