/* eslint-disable semi */
/* eslint-disable comma-dangle */
/* eslint-disable jsx-quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable no-trailing-spaces */
/* eslint-disable quotes */
/* eslint-disable prettier/prettier */
import React, {Component} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import Colors from '../../constants/colors';
import Header from '../../components/Header';
import HeaderText from '../../components/HeaderText';

import {ScrollView} from 'react-native-gesture-handler';
import LoginPrimaryButton from '../../components/LoginPrimaryButton';
import service from '../../services/transactions.service';
import { TransactionContext } from "../../states/transactions.state";
import FormatCurrency from "../../constants/currencyFormatter";
import colors from '../../constants/colors';


// change cont declaration name and export default name
class RecipientDetails extends Component {

    static contextType=TransactionContext;
    isViewLoaded=false;

    state={
        loading:false,
        charges:0,
        elevyCharges: 0,
        amount:0,
        actualAmount:0
    };

    componentDidMount=()=>{
        this.getCharges();
        this.getRecipient();
        this.isViewLoaded = true;

    };

    getCharges = async () => {
        try {
            const {transaction} = this.context.state;
            let data = {
                customerId: transaction.customerId,
                amount: transaction.amount,
                transaction_type: transaction.transaction_type,
                payment_account_type: transaction.payment_account_type,
                customerType: 'customers',
                payment_account_number: transaction.payment_account_number,
                payment_account_issuer: transaction.payment_account_issuer,
                recipient_account_issuer: transaction.recipient_account_issuer || ' ',
                recipient_account_number: transaction.recipient_account_number || ' ',
            }
             const response = await service.getCharges(data);
             if (response.success) {
                return this.setState({
                    charges:response.data.charges,
                    elevyCharges: response.data.elevyCharges,
                    amount:response.data.amount,
                    actualAmount:response.data.actualAmount
                });
            }
        } catch (err) {
            Alert.alert('Dear Customer',err.message);
        }
    }


    getRecipient=async()=>{
        try {
            const {transaction} = this.context.state;
            this.setState({loading:true});
            if (transaction.transaction_type === 'ECARDS' || transaction.transaction_type === 'AT') {
                return this.setState({
                    loading:false,
                    recipient_account_name: transaction.payment_account_name,
                });
            }
            const data = {
                DestBank:transaction.recipient_account_issuer,
                AccountToCredit:transaction.recipient_account_number,
                Narration:transaction.description,
            };
            const response = await service.getNec(data);
            if (!response.success) {
                Alert.alert('Dear Customer', response.message);
                return this.setState({
                    loading:false,
                    recipient_account_name: 'N/A',
                });
            }
            return this.setState({
                loading:false,
                recipient_account_name: response.data.NameToCredit,
            });
        } catch (err) {
            this.setState(
                {
                    loading: false,
                },
                ()=>{
                    Alert.alert('Dear Customer',err.message);
                    this.props.navigation.goBack();
                },
            );
        }
    };


    pay=async()=>{
        // console.log('here1');
        const {
            transaction,
            returnPage,
        } = this.context.state;
        const clearTransaction = this.context.clearTransaction;
        try {
            if (transaction.recipient_account_name === 'N/A') {
                throw Error ('Transaction cannot be completed, check account number and try again')
            }
            this.setState(
                {
                    loading:true,
                }
            );
            const response = await service.createTransaction(transaction);
            // console.log('CT:   ', response  );
            if (!response.success) {
                throw Error(
                    response.message
                );
            }
            switch (transaction.payment_account_type) {
                case 'card':
                    this.isViewLoaded && this.setState(
                        {
                            loading:false,
                        },()=>{
                            this.props.navigation.navigate(
                                'WebView',
                                {
                                    html:response.data,
                                    id:response.id,
                                    returnPage:returnPage,
                                }
                            );
                        }
                    );
                    break;
                default:
                    this.isViewLoaded && this.setState(
                        { loading:false },()=>{
                            clearTransaction(()=>{
                                    Alert.alert('Transaction received and processed',response.message);
                                    this.props.navigation.navigate(
                                        returnPage
                                    );
                                }
                            );
                        }
                    );
                    break;
            }
        } catch (err) {
            this.setState(
                {
                    loading:false,
                },()=>{
                    this.props.navigation.navigate(returnPage);
                    Alert.alert(
                        'Dear Customer',
                        err.message
                    );
                }
            );
        }
    }


    render=()=>{

        const {state,pushTransaction} = this.context;

        return (
            <View style={styles.screen}>
                <Header pressTo={()=>this.props.navigation.goBack()} />
                <ScrollView
                    contentContainerStyle={styles.scroll}
                    keyboardShouldPersistTaps="always"
                    keyboardDismissMode="on-drag">
                    <HeaderText title1='' title2="Recipient Details" />
                    <View style={styles.Body}>
                        <View style={styles.recipientContainer}>
                            <View
                            style={{
                                flexDirection:'row',
                                marginTop:25,
                                justifyContent:'space-between',
                            }}
                            >
                                <View style={styles.textContainer1}>
                                    <Text style={styles.text1}>Account Name</Text>
                                    <Text style={styles.text1}>Account Number</Text>
                                    <Text style={styles.text1}>Description</Text>
                                    <Text style={styles.text1}>Transfer Amount</Text>
                                    
                                </View>
                                <View style={styles.textContainer2}>
                                    <Text style={styles.text2}>
                                        {this.state.recipient_account_name ||
                                            'finding account...'}
                                    </Text>

                                    <Text style={styles.text2}>
                                        {state.transaction.recipient_account_number || state.transaction.payment_account_number }
                                    </Text>

                                    <Text style={styles.text2}>
                                        {state.transaction.description}
                                    </Text>
                                    
                                    <Text style={styles.text2}>
                                    {/* GH₵{FormatCurrency(this.state.actualAmount)} */}
                                    {(isNaN(FormatCurrency(this.state.actualAmount)) ? 'GH₵0.00' : `GH₵${FormatCurrency(this.state.actualAmount)}`) || 'loading'}
                                    </Text>
                                    
                                </View> 
                            </View>

                            <View
                            style={{
                                marginVertical:25,
                                flexDirection:'row',
                                backgroundColor:'#eee',
                                justifyContent:'space-between',
                            }}
                            >
                                <View style={styles.textContainer1}>
                                    <Text style={styles.text1}>E-Levy Fee</Text>
                                    <Text style={styles.text1}>PeoplesPay Fee</Text>
                                </View>
                                <View style={styles.textContainer2}>
                                    <Text style={styles.text2}>
                                    {(isNaN(FormatCurrency(this.state.elevyCharges)) ? 'GH₵0.00' : `GH₵${FormatCurrency(this.state.elevyCharges)}`) || 'loading'}

                                    </Text>
                                    <Text style={styles.text2}>
                                    {(isNaN(FormatCurrency(this.state.charges)) ? 'GH₵0.00' : `GH₵${FormatCurrency(this.state.charges)}`) || 'loading'}

                                    </Text>
                                </View>
                            </View>
                                
                            <View
                            style={{
                                flexDirection:'row',
                                width:'100%',
                                justifyContent:'space-between',
                                paddingHorizontal:100,
                                alignItems:'center',
                                marginVertical:20,
                            }}
                            >
                                <Text>
                                    Total:
                                </Text>
                                <Text
                                    style={{
                                        fontSize:25,
                                        color: colors.secondary,
                                        fontWeight:'600',
                                        marginLeft:20
                                    }}
                                >
                                {`GH₵${FormatCurrency(this.state.amount)}` ||
                                            'loading...'}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.buttonContainer}>
                            <LoginPrimaryButton
                                loading={this.state.loading}
                                title="Proceed"
                                pressTo={() => {
                                    if (this.state.recipient_account_name && this.state.recipient_account_name !== '') {
                                        pushTransaction(
                                            {
                                                recipient_account_name:this.state.recipient_account_name,
                                            },()=>{
                                                // console.log('here5');
                                                this.pay();
                                                // this.props.navigation.navigate(
                                                //     'ConfirmTransaction'
                                                // );
                                            }
                                        );
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        backgroundColor: 'white',
    },
    scroll: {
        flex: 1,
        backgroundColor: 'white',
        paddingHorizontal: 25,
        alignItems:'center',
    },
    Body: {
        // justifyContent: 'center',
        flex: 1,
        // alignItems:'center',
        // width:'100%',
    },
    recipientContainer: {
        flexDirection: 'column',
        backgroundColor: 'white',
        // justifyContent: 'center',
        // alignItems: 'center',
        // alignContent: 'center',
        marginTop: 10,
        marginBottom: 30,
        borderRadius:50,
        borderWidth:2,
        borderColor:'#eee',
        // opacity: 0.7,
    },
    textContainer1: {
        // flexDirection: 'row',
        // paddingVertical: 3,
        // backgroundColor: 'blue',
        flex:1,
    },
    text1: {
        paddingHorizontal: 20,
        // backgroundColor: Colors.accent,
        fontFamily: 'Roboto-Regular',
        textAlign: 'left',
        marginVertical: 5,
        fontSize: 13,
        opacity: 0.7,
    },
    text2: {
        // paddingHorizontal: 20,
        color: Colors.secondary,
        opacity: 0.7,
        fontFamily: 'Roboto-Medium',
        marginVertical: 3,
        fontSize: 13,
    },
    heading: {
        textAlign: 'left',
        paddingBottom: 10,
        marginBottom: 10,
        borderBottomWidth: 0.8,
        borderColor: Colors.primary,
        marginHorizontal: 30,
        fontFamily: 'Roboto-Bold',
        color: Colors.tetiary,
        opacity: 0.7,
    },
    dropdownContainer: {
        paddingVertical: 10,
        marginHorizontal: 30,
    },
    TxtInputContainer: {
        paddingHorizontal: 30,
    },
    buttonContainer: {
        // alignItems: 'center',
        paddingVertical: 20,
        paddingHorizontal: 30,
    },
    textContainer2: {
        alignItems:'flex-start',
        // backgroundColor:'yellow',
        paddingHorizontal:20,
        flex:1
    },
});

export default RecipientDetails;
