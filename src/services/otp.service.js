/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import API from '../constants/api';


const sendOTP = (phone) => fetch(
    `${API}/otp/sendotp`,
    {
        method: 'POST',
        body: JSON.stringify(
            {
                phoneNumber:phone
            }
        )
    }
).then(res=>res.json()).catch(err =>console.log('otpservice: ', err))



export default {
    sendOTP
};
