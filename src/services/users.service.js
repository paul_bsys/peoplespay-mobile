/* eslint-disable prettier/prettier */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import API from '../constants/api';


const customerLogin = (data) => fetch(
    `${API}/customers/login`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>new Error('Dear customer we cannot process your request,check your internet connection'));


const addCustomer = (data)=>fetch(
    `${API}/customers/new`,
    {
        method:'POST',
        body:JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>new Error('Dear customer we cannot process your request,check your internet connection'));

const getCustomer = (id)=>fetch(
    `${API}/customers/get/${id}`,
    {
        method: 'GET'
    }
).then(res => res.json()).catch(err=>err);

const updateCustomer = (data) => fetch(
    `${API}/customers/update`,
    {
        method: 'PUT',
        body: JSON.stringify(data)
    }
).then(res => res.json()).catch(err=>err);

const updatePassword = (data)=>fetch(
    `${API}/customers/update-password`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res => res.json()).catch(err=>err);


const forgotPassword = (data)=>fetch(
    `${API}/customers/forgotpassword`,
    {
        method:'POST',
        body: JSON.stringify(data)
    }
).then(res => res.json()).catch(err=>new Error('Dear customer we cannot process your request,check your internet connection'));


const verifyID = (data) => fetch(
    `${API}/verification/verify`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res => res.json()).catch(err=>err);

const getIds = () => fetch(
    `${API}/verification/get`,
    {
        method: 'GET'
    }
).then(res => res.json()).catch(err=>err);


export default {
    addCustomer,
    getCustomer,
    customerLogin,
    updateCustomer,
    updatePassword,
    forgotPassword,
    verifyID,
    getIds
};
