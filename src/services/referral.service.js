/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import API from '../constants/api';


// eslint-disable-next-line prettier/prettier
const generateCode = (data)=>fetch(
    `${API}/promotions/new`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);

const fetchCode = (id)=>fetch(
    `${API}/promotions/get/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);


const deleteCode = (id)=>fetch(
    `${API}/promotions/delete/${id}`,
    {
        method: 'DELETE'
    }
).then(res=>res.json()).catch(err=>err);

const summary = (id)=>fetch(
    `${API}/referals/summary/${id}`,
    {
        method: 'GET'
    }
).then(res=>res.json()).catch(err=>err);

const redeemCode = (data)=>fetch(
    `${API}/customers/redeem`,
    {
        method: 'POST',
        body: JSON.stringify(data)
    }
).then(res=>res.json()).catch(err=>err);


export default {
    generateCode,
    fetchCode,
    deleteCode,
    summary,
    redeemCode
};
