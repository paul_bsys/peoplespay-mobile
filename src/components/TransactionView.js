/* eslint-disable no-trailing-spaces */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React, {PureComponent} from 'react';
import {Text, Dimensions, StyleSheet, View} from 'react-native';

import SwiperFlatList from 'react-native-swiper-flatlist';
import Colors from '../constants/colors';
import FormatAmount from '../constants/currencyFormatter';

export default class App extends PureComponent {

  render() {
    const data = this.props.data;
    const getData = (type)=>{
      try {
        return this.props.toggle ? this.props.data[type][0].totalAmount : 'X.00';
      } catch (err) {
        return 0;
      }
    };

    return (
      <View style={styles.container}>
        {/* <Text style={[styles.title, {color: 'white'}]}>
          Recent Transactions
        </Text> */}
        <SwiperFlatList autoplay autoplayDelay={5} autoplayLoop index={2}>
          <View style={styles.child}>
            {/* <Text style={styles.text}>1.</Text> */}
            <Text style={styles.text}>Send Money</Text>
            <Text style={styles.text2}>GH₵{getData('SM') !== 'X.00' ? FormatAmount(getData('SM')) : 'X.00'}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>2.</Text> */}
            <Text style={styles.text}>Airtime Topup</Text>
            <Text style={styles.text2}>GH₵{getData('AT') !== 'X.00' ? FormatAmount(getData('AT')) : 'X.00'}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>3.</Text> */}
            <Text style={styles.text}>Gift Cards</Text>
            <Text style={styles.text2}>GH₵{getData('ECARDS') !== 'X.00' ? FormatAmount(getData('ECARDS')) : 'X.00'}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>4.</Text> */}
            <Text style={styles.text}>Pay Bills</Text>
            <Text style={styles.text2}>GH₵{getData('PB') !== 'X.00' ? FormatAmount(getData('PB')) : 'X.00'}</Text>
          </View>
          <View style={styles.child}>
            {/* <Text style={styles.text}>5.</Text> */}
            <Text style={styles.text}>GHQ</Text>
            <Text style={styles.text2}>GH₵{getData('QRP') !== 'X.00' ? FormatAmount(getData('QRP')) : 'X.00'}</Text>
          </View>
        </SwiperFlatList>
      </View>
    );
  }
}


export const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    width:'100%',
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    height:40
  },
  child: {
    width:width / 1.1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    // marginHorizontal:2
  },
  title: {
    textAlign: 'center', 
    color:'#fff',
    fontWeight:'600'
  },
  text: {
    textAlign:'center', 
    color:'#fff',
    fontWeight:'600'
  },
  text2:{
    color:'#fff',
    marginRight:15,
    fontWeight:'600'
  }
});
