/* eslint-disable no-trailing-spaces */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
//This is an example code for Navigation Drawer with Custom Side bar//
import React, {Component, Platform} from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Share,
  Alert
} from 'react-native';


import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Icons from 'react-native-vector-icons/FontAwesome5';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import colors from '../constants/colors';
import Color from '../constants/colors';
import {AuthConsumer} from '../states/auth.state';

const MY_COMP_HIT_SLOP = {top: 30, left: 30, right: 30, bottom: 30};
export default class CustomSidebarMenu extends Component {


  onShare=async()=>{
    try {
      const result = await Share.share({
        message:
          'I’m using PeoplesPay to pay smartly. Click here to install for FREE. Android~https://play.google.com/store/apps/details?id=com.bsystems.peoplespay and Iphone~https://apps.apple.com/gh/app/peoplespay/id1543677240',
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      Alert.alert(
        'Something went wrong',
        'We could not share your request..'
      );
    }
  };


  render() {

    const items = [
      {
        navOptionThumb: 'home',
        navOptionThumbType: 'font-awesome',
        navOptionName: 'Home',
        screenToNavigate: 'Home',
        navOptionThumbColor: 'red',
      },
      {
        navOptionThumb: 'history',
        navOptionThumbType: 'font-awesome',
        navOptionName: 'Transactions',
        screenToNavigate: 'History',
        navOptionThumbColor: 'red',
      },
      {
        navOptionThumb: 'bank',
        navOptionThumbType: 'font-awesome',
        navOptionName: 'Merchant',
        screenToNavigate: 'MerchantLogin',
        navOptionThumbColor: 'red',
      },
      // {
      //   navOptionThumb: 'users',
      //   navOptionThumbType: 'font-awesome',
      //   navOptionName: 'Beneficiaries',
      //   screenToNavigate: 'BankBeneficiary',
      //   navOptionThumbColor: 'red',
      // },
      {
        navOptionThumb: 'edit',
        navOptionThumbType: 'feather',
        navOptionName: 'Profile',
        screenToNavigate: 'Profile',
      },
      {
        navOptionThumb: 'form-textbox-password',
        navOptionName: 'Change Password',
        screenToNavigate: 'Password',
        navOptionThumbType: 'material-community',
      },
      {
        navOptionThumb: 'support',
        navOptionName: 'Help',
        screenToNavigate: 'Help',
        navOptionThumbType: 'simple-line-icon',
      },
      {
        navOptionThumb: 'gavel',
        navOptionName: 'Legal',
        screenToNavigate: 'Legal',
        navOptionThumbType: 'font-awesome',
      }
    ];

    return (
      <AuthConsumer>
        {({state, logout}) => {
          const {user} = state;
          return (
            <View style={styles.sideMenuContainer}>
              
                <View
                  style={styles.linearGradient}
                  // colors={['#C81B44', '#ba3d58']}
                  // start={{ x: 0, y: 0.5 }}
                  // end={{ x: 1, y: 0 }}
                  // locations={[0.4, 0.65]}
                >
                  <TouchableOpacity
                    style={{width:'100%'}}
                    onPress={() => this.props.navigation.navigate('Profile')}
                  >
                    <View
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginBottom:5,
                        marginTop:20
                      }}>
                      <Icon
                        reverse
                        name="user"
                        type="antdesign"
                        color="#fff"
                        size={Dimensions.get('window').height > 700 ? 30 : 20}
                        iconStyle={{color: 'black'}}
                        containerStyle={{
                          borderRadius: 100,
                        }}
                      />
                    </View>
                    <Text
                      style={{
                        flexWrap:'wrap',
                        fontWeight: 'normal',
                        fontSize: Dimensions.get('window').height * 0.020,
                        color: 'white',
                        textAlign: 'center',
                      }}>
                      {user.fullname}
                    </Text>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontWeight: 'normal',
                        marginVertical:5,
                        color: 'white',
                        flexWrap:'wrap'
                      }}>
                      {user.email}
                    </Text>
                  </TouchableOpacity>
                </View>
              
              {/*Divider between Top Image and Sidebar Option*/}
              <View
                style={{
                  width: '100%',
                  height: 1,
                  backgroundColor: Color.secondary,
                  marginTop: 15,
                }}
              />
              {/*Setting up Navigation Options from option array using loop*/}
              <ScrollView style={{width: '100%'}}>
                <View style={{flex: 1}}>
                  {items.map((item,key)=>(
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        backgroundColor:
                          global.currentScreenIndex === key
                            ? Color.secondary
                            : '#ffffff',
                        // borderBottomWidth: 1,
                        // borderBottomColor: 'rgba(0,0,0,0.2)',
                        // height: Dimensions.get('window').height * 0.1,
                      }}
                      key={key}>
                      <View style={{marginRight: 10, marginLeft: 20}}>
                        <Icon
                          name={item.navOptionThumb}
                          size={24}
                          color={
                            global.currentScreenIndex === key
                              ? 'white'
                              : Color.secondary
                          }
                          type={item.navOptionThumbType}
                        />
                      </View>
                      <Text
                        style={{
                          paddingVertical:
                            Dimensions.get('window').height * 0.022,
                          fontWeight: 'normal',
                          fontSize: Dimensions.get('window').height * 0.018,
                          color:
                            global.currentScreenIndex === key
                              ? 'white'
                              : 'black',
                        }}
                        onPress={() => {
                          global.currentScreenIndex = key;
                          this.props.navigation.navigate(item.screenToNavigate);
                        }}>
                        {item.navOptionName}
                      </Text>
                    </View>
                  ))}

                    <TouchableOpacity
                        style={styles.sbutton}
                        onPress={this.onShare.bind(this)}>
                          <Icons 
                            name="share"
                            size={24}
                            style={{marginRight:10}}
                            color={colors.secondary}
                          />
                        <Text
                          style={{
                            fontFamily: 'Roboto-Regular',
                            fontSize: Dimensions.get('window').height * 0.018,
                          }}>
                          Share App
                        </Text>
                    </TouchableOpacity>

                  
                </View>
              </ScrollView>
              <View
                    style={{
                      justifyContent: 'flex-end',
                      width: '100%',
                      alignSelf:'flex-end'
                    }}>

                    <View
                      style={{
                        width: '100%',
                        borderColor: Colors.tertiary,
                        borderTopWidth: 1,
                      }}>
                      <TouchableOpacity
                        style={{
                          marginTop:15,
                          marginBottom:5,
                          flexDirection: 'row',
                          paddingVertical: 5,
                          paddingHorizontal: 20,
                          alignItems:'center'
                        }}
                        onPress={()=>logout()}>
                        <Icon
                          name="logout"
                          size={26}
                          color={Color.secondary}
                          type="material-community"
                          style={{marginRight:10}}
                        />
                        <Text
                          style={{
                            fontFamily: 'Roboto-Bold',
                            fontSize: Dimensions.get('window').height * 0.022,
                          }}>
                          SIGN OUT
                        </Text>
                      </TouchableOpacity>
                    </View>

                  </View>
            </View>
          );
        }}
      </AuthConsumer>
    );
  }
}
const styles = StyleSheet.create({
  sideMenuContainer: {
    width: '100%',
    height: '100%',
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 20,
  },
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: 150,
    height: 150,
    marginTop: 20,
    borderRadius: 75,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  cbutton:{
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginTop:10
  },
  sbutton:{
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginTop:10
  },
  tbutton:{
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 20,
    marginTop:10,
    borderBottomWidth:1, 
    borderBottomColor: 'rgba(0,0,0,0.2)',
    paddingBottom:11
  },
  linearGradient:{
    width:'100%',
    // marginVertical:5,
    // borderWidth:2,
    // borderColor:'#ddd',
    backgroundColor:colors.secondary,
    // shadowColor:'rgb(0,0,0)',
    // shadowOpacity:0.1,
    // shadowOffset:'30px',
    // borderRadius:16,
    // padding:10,
    // transform: [{ scale: 0.9 }],
    // paddingVertical:10,
    height:160
}
});
