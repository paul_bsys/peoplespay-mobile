/* eslint-disable jsx-quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import * as React from 'react';
import {Text, StyleSheet, View, TouchableOpacity,Image,Dimensions,ScrollView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import issuer from '../services/issuers.service';
import {Icon} from 'react-native-elements';
import { Platform } from 'react-native';
import Header from '../components/Header';
import { TransactionContext } from '../states/transactions.state';
import {DrawerActions} from 'react-navigation-drawer';



const {width,height} = Dimensions.get('screen');



export class CustomExample extends React.Component {

  static contextType=TransactionContext;
  isLoaded=false;

  state={
    loading:false,
    issuers:[],
    type:'',
    page:''
  };


  next(data){
    try {
      const page = this.props.navigation.getParam('page');
      const _data = this.props.navigation.getParam('data');
      const key = this.props.navigation.getParam('key');
      if (typeof page !== 'string') {return;}
      const {pushTransaction,state} = this.context;
      let _d = {};
      switch (key[2]) {
        case true:
          _d = {
            [key[0]]:data.id,
            [key[1]]:data.shortName,
            [key[2]]:data.image
          };
          break;
        default:
          _d = {
            [key[0]]:data.id,
            [key[1]]:data.shortName,
          };
          break;
      }
      pushTransaction(_d,()=>this.props.navigation.navigate(page,_data));
    } catch (err) {}
  }



  componentDidMount=()=>{
    try {
      this.isLoaded = true;
      const type = this.props.navigation.getParam('type');
      this.setState(
        {
          issuers:[],
          type:type
        },()=>{
          this.loadCache();
          this.getIssuers();
        }
      );
    } catch (err) {}
  };


  componentWillUnmount(){
    this.isLoaded = false;
  }


  async loadCache(){
    try {
      const _type = this.state.type;
      const cache = await AsyncStorage.getItem('ppay-issuers');
      if (typeof cache === 'string'){
        const data = JSON.parse(cache);
        const issuers = data.filter(({type})=>type === _type);
        this.isLoaded && this.setState({ issuers:issuers });
      }
    } catch (err) {}
  }


  getIssuers=async()=>{
    try {
      this.setState({loading:true,issuers:[]});
      const response = await issuer.getIssuers();
      if (!response.success){
        throw Error(
          'Oops'
        );
      }
      await AsyncStorage.setItem(
        'ppay-issuers',
        JSON.stringify(response.data)
      );
      this.setState(
        {
          loading:false,
        },this.loadCache
      );
    } catch (err) {
      this.setState(
        {
          loading: false
        }
      );
    }
  };

  render() {
    return (
      <View style={{height:height,backgroundColor:'white',}}>
        <ScrollView contentContainerStyle={{marginTop:5,...Platform.select({ 'android':{ paddingBottom:height / 8 } })}}>
            <Header 
              text="Choose Account" 
              pressTo={()=>this.props.navigation.goBack()} 
              iconRight={true}
              iconRName='bars'
              iconRPress={()=>{
                this.props.navigation.dispatch(
                    DrawerActions.toggleDrawer(),
                );
            }}
            />
              {this.state.issuers.map((issuer,i)=>{
                return (
                  <TouchableOpacity
                    key={i.toString()}
                    onPress={()=>this.next(issuer)}
                    style={[styles.Option,]}>
                        <Image style={{width:50,height:50}} source={{ uri:issuer.image }}/>
                        <Text style={{marginLeft:15,fontSize:17,fontWeight:'200', width:'90%'}}>{ issuer.name }</Text>
                  </TouchableOpacity>
                );
              })}
        </ScrollView>
      </View>
    );
  }

  renderOption(settings) {
    const {item, getLabel} = settings;
    return (
      <View style={styles.optionContainer}>
        <View style={styles.innerContainer}>
          <Text
            style={{
              color: item.color,
              alignSelf: 'flex-start',
              fontFamily: 'Roboto-Bold',
              overflow: 'hidden',
            }}>
            {getLabel(item)}
          </Text>
          {item.account_number && (
            <Text
              style={{
                color: item.color,
                alignSelf: 'flex-end',
                fontFamily: 'Roboto-Bold',
                overflow: 'hidden',
              }}>
              {`********${item.account_number.substring(
                item.account_number.length - 4,
                item.account_number.length,
              )}`}
            </Text>
          )}
        </View>
      </View>
    );
  }

  renderField(settings) {
    const {selectedItem, defaultText, getLabel, clear} = settings;
    return (
      <View style={styles.container}>
        <View>
          {!selectedItem && (
            <View
              style={{
                ...styles.innerContainer,
                justifyContent: 'space-between',
              }}>
              <Text style={[styles.text]}>{defaultText}</Text>
              <View style={styles.clearButton}>
                <Icon name="downcircleo" type="antdesign" />
              </View>
            </View>
          )}
          {selectedItem && (
            <View
              style={{
                ...styles.innerContainer,
                flexDirection: 'row-reverse',
                justifyContent: 'space-between',
                overflow: 'hidden'
              }}>
              <TouchableOpacity style={styles.clearButton} onPress={clear}>
                <Icon name="closecircleo" type="antdesign" />
              </TouchableOpacity>
              <Text style={[styles.text]}>
                {getLabel(selectedItem).length > 20
                  ? getLabel(selectedItem).substring(0, 18) + ' ...'
                  : getLabel(selectedItem)}
              </Text>
            </View>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderColor: 'grey',
    borderWidth: 1,
    ...Platform.select(
      {
        ios:{
          paddingVertical:7,
          borderRadius: 20
        },
        android:{
          paddingVertical:1,
          borderRadius: 15,
        }
      }
    )
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    width: '100%',
    justifyContent: 'space-between',
  },
  Option: {
    flexDirection:'row',
    alignItems:'center',
    marginHorizontal:10,
    marginVertical:5,
    height:90,
    width:'95%',
    paddingVertical:20,
    paddingHorizontal:10,
    borderRadius:15,
    borderWidth:2,
    borderColor:'#eee',
  },
  text: {
    fontSize: 18,
    padding: 10,
    color: 'grey',
  },
  headerFooterContainer: {
    padding: 10,
    alignItems: 'center',
  },
  clearButton: {
    backgroundColor: 'white',
    borderRadius: 5,
    marginRight: 10,
    padding: 10,
    paddingLeft: 20,
  },
  optionContainer: {
    padding: 10,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    borderRadius: 0,
    fontFamily: 'Roboto-Regular',
  },
  optionInnerContainer: {
    flex: 1,
    flexDirection: 'row',
    fontFamily: 'Roboto-Bold',
  },
  box: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
});
