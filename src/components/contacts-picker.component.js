/* eslint-disable jsx-quotes */
/* eslint-disable no-trailing-spaces */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable comma-dangle */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
    Text,
    View,
    FlatList,
    StyleSheet,
    Modal,
    TouchableOpacity,
    SafeAreaView,
    StatusBar,
    Platform,
    TextInput
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import Iconx from 'react-native-vector-icons/Feather';
import Contacts from 'react-native-contacts';
import colors from '../constants/colors';
import { PermissionsAndroid } from 'react-native';



export default class LazyContactPicker extends Component {

    state={
        numbers:[],
        contacts:[],
        _temp:[],
    }


    onsearch=(v)=>{
        try {
            const filter = this.state.contacts.filter(cont=>`${cont.givenName} ${cont.familyName}`.indexOf(v) > -1);
            this.setState({contacts:filter});
        } catch (err) {}
    }

    getContact(){
        if (Platform.OS === 'android'){
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
                {
                  'title': 'Contacts',
                  'message': 'PeoplesPay would like to view your contacts.',
                  'buttonPositive': 'Please accept'
                }
              ).then((res)=>{
                Contacts.getAll().then(contacts=>{
                    // console.log(cons);
                    contacts.sort((a,b) => (a.givenName > b.givenName) ? 1 : ((b.givenName > a.givenName) ? -1 : 0));
                    this.setState(
                        {
                            contacts:contacts,
                            _temp:contacts,
                            numbers:[]
                        }
                    );
                });
              });
        } else {
            Contacts.getAll().then(contacts=>{
                // console.log(cons);

                contacts.sort((a,b) => (a.givenName > b.givenName) ? 1 : ((b.givenName > a.givenName) ? -1 : 0));

                this.setState(
                    {
                        contacts:contacts,
                        _temp:contacts,
                        numbers:[]
                    }
                );
            });
        }
    }

    render() {
        return (
            <Modal
                animationType="slide"
                onDismiss={()=>null}
                presentationStyle="fullScreen"
                visible={this.props.openModal}
                onShow={this.getContact.bind(this)}

            >
                <StatusBar barStyle="dark-content"/>
                <SafeAreaView style={styles.modal}>
                    <View style={{
                        paddingHorizontal:30,
                        paddingVertical:20,
                        flexDirection:'column',
                        // alignItems:'center',
                        // borderRadius:5,
                        // borderColor:'#ddd',
                        // borderBottomWidth:1
                    }}>
                        <TouchableOpacity onPress={()=>{
                            if (this.state.numbers.length > 0){
                                this.setState(
                                    {
                                        numbers:[]
                                    }
                                );
                            } else {
                                this.props.closeModal();
                            }
                        }}>
                            <Icon
                                name="times"
                                size={25}
                                color={colors.secondary}
                            />
                        </TouchableOpacity>
                        <View
                            style={{width:'98%', flexDirection:'row', borderRadius:25, padding:5, backgroundColor:'#f7f7f5', alignItems:'center', marginTop:10}}
                        >
                            <View style={{flexDirection:'row', width:'90%', alignItems:'center'}}>
                                <Iconx
                                    name='search'
                                    size={16}
                                    color='gray'
                                />
                                <TextInput
                                    onChangeText={(text)=>{
                                        text === '' ? this.setState({contacts:this.state._temp}) : this.onsearch(text);
                                        // this.setState({txt:''});
                                    }}
                                    style={{ marginLeft:3, backgroundColor:'transparent', marginVertical:0, width:'90%'}}
                                    placeholder="Search name..."
                                />
                            </View>
                            
                            {/* <TouchableOpacity
                                style={{flexDirection:'row', alignItems:'center'}}
                                onPress={() => this.setState({txt:''})}
                            >
                                <Icon
                                    name="times"
                                    size={16}
                                    color='gray'
                                />
                            </TouchableOpacity> */}
                        </View>
                        
                    </View>
                    <FlatList
                        style={styles.list}
                        extraData={this.state}
                        data={this.state.numbers.length > 0 ? this.state.numbers : this.state.contacts}
                        keyExtractor={(v,i)=>i.toString()}
                        renderItem={({item})=>{
                            switch (this.state.numbers.length > 0) {
                                case true:
                                    return (
                                        <TouchableOpacity
                                            style={styles.listItem}
                                            onPress={()=>{
                                                this.setState(
                                                    {
                                                        numbers:[]
                                                    },()=>{
                                                        this.props.getContact(item.number);
                                                    }
                                                );
                                            }}
                                        >
                                            <Text style={styles.text}>
                                                {item.number}
                                            </Text>
                                        </TouchableOpacity>
                                    );
                                default:
                                    return (
                                        <TouchableOpacity
                                            style={styles.listItem}
                                            onPress={()=>{
                                                this.setState(
                                                    {
                                                        numbers:item.phoneNumbers
                                                    }
                                                );
                                            }}
                                        >
                                            <Text style={styles.text}>
                                                {`${item.givenName} ${item.familyName}`}
                                            </Text>
                                            
                                            {/* <View style={styles.contItem}>
                                                <View style={styles.countCont}>
                                                    <Text style={{color:'#fff'}}>{item.phoneNumbers.length}</Text>
                                                </View>
                                                <Text style={styles.text}>
                                                    {`${item.givenName} ${item.familyName}`}
                                                </Text>
                                            </View>
                                            <Text style={styles.text1}>
                                                {item.phoneNumbers.map(num=>num.number).join('\n')}
                                            </Text> */}
                                        </TouchableOpacity>
                                    );
                            }
                        }}
                    />
                </SafeAreaView>
                <Text style={{width:'100%', textAlign:'center', borderTopWidth:0.8, paddingVertical:10 }}>
                    {this.state.contacts.length} contact{this.state.contacts.length > 1 ? 's' : ''}
                </Text>
            </Modal>
        );
    }
}





const styles = StyleSheet.create({
    modal:{
        flex:1,
        backgroundColor:'white'
    },
    list:{
        padding:20,
        paddingHorizontal:2
    },
    text:{
        fontSize:16,
        paddingHorizontal:20,
        color:'black',
        fontWeight:'600'
    },
    text1:{
        fontSize:20,
        color:'#6d6d6d',
        paddingHorizontal:50
    },
    listItem:{
        borderTopColor:'lightgray',
        borderTopWidth:0.8,
        paddingVertical:10
    },
    contItem:{
        flexDirection:'row',
        alignItems:'center',
        marginVertical:10
    },
    countCont:{
        borderRadius:50,
        width:30,
        height:30,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:colors.secondary
    }
});



// export default {
//     NumbersComponent,
//     ContactsPickerComponent
// }
