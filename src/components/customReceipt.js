import React from 'react'

const customReceipt = (data) => {
  return `
  <div
  style="
      display: flex;
      width: 100%;
      flex-direction: column;
      align-items: center;
      height: 100%;
      gap: 1.5rem;      
      padding-top: 1rem;  
  "
  >

      <span
      style="
          font-size: 2rem;
          color: #c81b44;

      "
      >Transaction Receipt</span>

      <div
      style="
          display: flex;
          flex-direction: column;
          width: 80%;
          gap: 2rem;

      "
      >

          <div
          style="
              display: flex;
              flex-direction: column;
              width: 100%;
              gap:0.5rem;
              
          "
          >
              <span
              style="
                  font-size: 1.35rem;
              "
              >
              Amount
              </span>
              <span
              style="
                  font-size: 2rem;
                  color: #c81b44;
                  font-weight:500;
              "
              >
              GH₵${data.amount}</span>
          </div>

          <div
          style="
              display: flex;
              flex-direction: column;
              width: 100%;
              gap: 1.5rem;
              
          "
          >

             
                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Status</span>
                      <span style="font-size: 1.1rem;">${data.status}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Date</span>
                      <span style="font-size: 1.1rem;">${data.date}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Transaction</span>
                      <span style="font-size: 1.1rem;">${data.transaction}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Payment Account</span>
                      <span style="font-size: 1.1rem;">${data.payment_account}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Transferred From</span>
                      <span style="font-size: 1.1rem;">${data.transFrom}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Transferred To</span>
                      <span style="font-size: 1.1rem;">${data.transTo}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Reference</span>
                      <span style="font-size: 1.1rem;">${data.reference}</</span>

                  </div>

              
              
          </div>

          <div
          style="
              display: flex;
              flex-direction: column;
              width: 100%;
              gap: 1rem;
              margin-top: 2rem;
              
          "
          >
              <span style="font-size: 1.1rem; width:100%; color:#c81b44; padding:0.2rem">Payment Details</span>

              <div
              style="
                  display: flex;
                  flex-direction: column;
                  width: 100%;
                  gap: 1.5rem;
                  
              "
              >

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Total Amount</span>
                      <span style="font-size: 1.1rem;">GH₵${data.amount}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">E-Levy Charge</span>
                      <span style="font-size: 1.1rem;">-GH₵${data.elevy}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Peoples Pay Fee</span>
                      <span style="font-size: 1.1rem;">-GH₵${data.ppFee}</</span>

                  </div>

                  <div
                      style="
                          display: flex;
                          width: 100%;
                          flex-direction: row;
                          align-items: center;
                          justify-content: space-between;
                      "
                  >
                      <span style="font-size: 1.1rem;">Transfer Amount</span>
                      <span style="font-size: 1.1rem;">GH₵${data.actualAmount}</</span>

                  </div>

                 

                

              </div>
          </div>
          

      </div>

  </div>
  `
}

export default customReceipt

