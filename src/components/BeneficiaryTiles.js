/* eslint-disable no-trailing-spaces */
/* eslint-disable comma-dangle */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image,Dimensions, Platform} from 'react-native';
import Colors from '../constants/colors';
import IconButton from './IconButton';
import {Icon} from 'react-native-elements';
const {height} = Dimensions.get('screen');

const BeneficiaryTiles = props=>{
  const renderIcon = ()=>{
    switch (props.account_type) {
      case 'momo':
        return {
          name:'mobile',
          icon:'entypo',
        };
      case 'token':
      case 'card':
        return {
          name:'credit-card',
          icon:'font-awesome-5',
        };
      case 'bank':
        return {
          name:'bank',
          icon:'material-community',
        };
      case 'wallet':
        return {
          name:'wallet',
          icon:'font-awesome-5',
        };
      default:
        return {
          name:'',
          icon:'',
        };
    }
  };
  return (
    <TouchableOpacity disabled={props.disabled} activeOpacity={0.8} onPress={props.pressTo}>
      <View style={styles.CardThumbnailContainer}>
        <View style={styles.Container1}>
          <View style={styles.ImageContainer}>
            {props.imageUri?.uri ? <Image
              style={styles.Image}
              source={props.imageUri}
              resizeMode="contain"
            /> : <Icon
            name={renderIcon().name}
            type={renderIcon().icon}
            size={35}
            color={Colors.secondary}
            />}
          </View>
          <View style={styles.TextContainer}>
            <Text style={styles.TitleText}>{props.title}</Text>
            <View style={{flexDirection:'row', alignItems:'center', justifyContent:'space-between', marginTop:15}}>
              <Text style={styles.MainText}>{props.issuer}</Text>
              {/* <Text style={styles.MainText}>{'******' + props.number.substring(props.number.length - 4,props.number.length)}</Text> */}
            </View>
          </View>

          
        </View>
        {props.showDelete && <View style={styles.button}>
            <IconButton name={props.deleteIcon || 'trash-alt'} onPress={props.delete}/>
          </View>}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  CardThumbnailContainer: {
    flexDirection:'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.accent,
    marginHorizontal:10,
    marginVertical:10,
    width:'95%',
    paddingVertical:20,
    paddingHorizontal:10,
    borderRadius:15,
    borderWidth:2,
    borderColor:'#eee',
    height:100
  },
  Container1: {
    // height:'100%',
    flexDirection: 'row',
    alignItems:'center',
    // justifyContent:'space-between',
    backgroundColor: Colors.accent,
  },
  ImageContainer: {
    flexDirection:'column',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: Colors.accent,
    height:'95%',
    width:50,
    overflow:'hidden',
    // borderColor:'#ddd',
    // borderRadius:50,
    // borderWidth:1
  },
  Image: {
    height:'100%',
    width:'100%',
    alignSelf:'center',
  },
  TextContainer: {
    // width:'100%',
    color:Colors.secondary,
    paddingHorizontal:10,
    flexDirection:'column',
    justifyContent:'center',
    height:'100%'
  },
  MainText: {
    fontWeight:'300',
    opacity: 0.7,
    color:'#000',
  },
  TitleText: {
    fontFamily:'Roboto-Bold',
    fontSize:17,
    color: Colors.secondary,
    fontWeight:'200',
  },
  BottomIcon: {
    opacity: 0.7,
    justifyContent: 'flex-end',
    alignSelf: 'flex-end',
  },
  button:{
    height:'100%',
    // backgroundColor:'yellow',
    justifyContent:'flex-end',
    alignItems:'flex-end',
    position:'absolute',
    right:-10,
    bottom:-5,
    // zIndex:100000
  },
});

export default BeneficiaryTiles;
